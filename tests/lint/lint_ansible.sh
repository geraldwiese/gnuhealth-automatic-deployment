#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

for playbook in playbooks/*.yml; do
	echo "Linting $playbook"
	ansible-lint -p $playbook -x var-naming[no-role-prefix]
done

echo "No problems found during linting"
