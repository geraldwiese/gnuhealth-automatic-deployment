.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

nginx
=====

Nginx is used as reverse proxy / web server.

Basically the nginx package is installed and site templates for public access are used for HTTP or HTTPS.
For uWSGI it might be necessary to add the Nginx user to the `uwsginginx` group.

Nginx will only be accessible using TLS 1.2 & 1.3 because older versions have known security issues.

Apart from setting paths for certificate and keys for Nginx itself and the application (for verification) the site template contains numerous connection options for accessing the application:

- In case of GNU Health:

  - Using a UNIX socket if Nginx and GNU Health are on the same system

  - Using the uWSGI protocol if having separate systems but the intranet is considered secure (or performance considered too poor)

  - Using HTTPS if having separate systems but internal communication should be encrypted as well (default for separate systems)

- In case of Orthanc:

  - HTTP if it's the same system

  - HTTPS by default if having separate systems

The domain names `nginx_rproxy_domain` and `nginx_application_domain` have to be set correctly:
The first one is used for the certificate and the second one is where the application server is expected.

If setting `nginx_http_redirect` a redirect from HTTP to HTTPS will be created as well.

In order to modify or extend the Nginx sites configuration have a look at the templates in `roles/nginx/templates/`.

| Nginx documentation:
| https://docs.nginx.com/

- **Parameters**:

  - nginx_rproxy_domain: Domain name where this reverse proxy is reachable, entered in the certificate if using HTTPS
  - nginx_application_domain: Domain name of the application server to forward to
  - nginx_http_redirect: If true create a HTTP redirect to HTTPS
  - nginx_https: If true enable HTTPS sites
  - nginx_sites_https: HTTPS sites to enable
  - nginx_call_certificate_role: If true call certificate role to handle certificate and key
  - nginx_http: If true and nginx_http_redirect is false, enable HTTP sites
  - nginx_sites_http: HTTP sites to enable

- **Defaults**:

  - nginx_cert_filename: Filename of certificate
  - nginx_key_filename: Filename of key
  - nginx_application_https: If true connect to application server using HTTPS
  - nginx_uwsgi: If true uWSGI is used at least for one site
  - nginx_uwsgi_unix_socket: If true a UNIX socket is used for uWSGI at least for one site
  - nginx_copy_app_cert: If true copy certificate from application server
  - nginx_app_cert_path: Path of certificate from application server
  - nginx_app_cert_path_ownca: Default path to pass to `nginx_app_cert_path` when using custom CA
  - nginx_app_cert_path_local: Default path to pass to `nginx_app_cert_path` when using existing certificate from Ansible controller
  - nginx_app_cert_path_remote: Default path to pass to `nginx_app_cert_path` when using existing certificate already on server
  - nginx_copy_crl: Whether Nginx should hold a Certificate Revocation List (CRL) for application server in case of custom CA
  - nginx_crl_path: Path of the CRL
  - nginx_ownca: If true a Certificate Authority in involved and proxy settings are configured likewise
  - nginx_fail2ban: If Fail2ban is used together with Nginx
  - nginx_fail2ban_main_line: Configure limit on requests with Fail2Ban and Nginx
  - nginx_fail2ban_location_line: Configure limit on requests with Fail2Ban and Nginx
  - nginx_letsencrypt: If Let's Encrypt is used together with Nginx
  - nginx_package_version_pinning: If the package version is pinned to the given version
  - nginx_package_version: Package version
  - nginx_template_comment: Comment put on top of every template delivered by Ansible
  - nginx_include_site_config: If another config file should be included for the sites
  - nginx_include_site_config_path: Path of config to be included

- **Variables**:

  - nginx_ssl_paths: Directories for certificates, keys and CRLS
  - nginx_config_dir: Directory for Nginx configuration dependent from OS family
  - nginx_dir_sites_available: Directory for available sites dependent from OS family
  - nginx_dir_sites_enabled: Directory for active sites dependent from OS family
  - nginx_user: Name of OS user used by Nginx dependent from OS family
  - nginx_uwsgi_group_name: Name of the group to be created for Nginx user & uWSGI user in order to have proper UNIX socket permissions (if necessary)
