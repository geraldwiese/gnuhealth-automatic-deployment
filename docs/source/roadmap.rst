.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Roadmap
=======

Todos
-----

    - Synchronize Vanilla & Ansible installation
    - Extended example for productive use including Zabbix
    - Add DHIS2 to roles & playbooks
	- Extend test cases
	- Continue polishing

Related issues
--------------

Since this project uses a lot of other free or open source software there are quite a lot issues involved.
For some of them workarounds were introduced that should be removed once related issues are fixed in the other projects and those fixes went upstream.
Those issues include:

* openSUSE Leap using Python 3.6 although it reached end of life

* openSUSE Leap no way to get pg_config executable without installing the whole PostgreSQL server:

  https://forums.opensuse.org/showthread.php/567472-trying-to-install-pg_config-binary-Can-t-find-the-right-package
* Debian Fail2Ban default jail for SSH not working:

  https://github.com/fail2ban/fail2ban/issues/3292#issuecomment-1678844644
* Debian Orthanc builtin HTTPS not working:

  https://discourse.orthanc-server.org/t/builtin-https-not-working-on-ubuntu-22-04/3922

* `become_user: nonroot doesn't work on FreeBSD #67245`

  https://github.com/ansible/ansible/issues/67245#issuecomment-631587263

* Barman on Debian places `postgresql.auto.conf` at PostgreSQL server with `restore_command` without the previously configured `--port` option

* uWSGI on FreeBSD can't run if installed with pip (always seg fault)

If one of those issues was resolved but the workaround here is still present feel free to open an issue or write an email to wiese@gnuhealth.org
