.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Roles
=====

In the following sections you can find information about all the roles used in this project.

For all of those roles variables are handled in three different kinds:

- **Parameters** are kept both in roles/<role_name>/defaults/main.yml and inventories/dev/group_vars/. They are likely to be configured in the inventory by users but still defined inside the roles directory in case the role is used independently. There are tests to ensure this consistency.

- **Defaults** are only kept in roles/<role_name>/defaults/main.yml. They are not likely to be configured. Many of them are only taking variables from other roles with default fallbacks. Others are seen as neglectable and were not put into the inventory in order to keep it overseeable.

- **Variables** are only kept in roles/<role_name>/vars/main.yml. They are only thought to be configured by maintainers of the project. However there could still be scenarios where users want to touch them e.g. when package names, package dependencies or operating system versions evolve and maintainers did not update yet.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   barman
   certificate
   check
   fail2ban
   gnuhealth
   gnuhealth-client
   gpg
   locale
   logwatch
   mygnuhealth
   nftables
   nginx
   orthanc
   postfix
   postgresql
   restic
   ssh
   ssmtp
   systemd-alert
   test-server
   thalamus
   timezone
   unattended-upgrades
   uwsgi
   zabbix
