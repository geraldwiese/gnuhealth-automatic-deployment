.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Welcome to GNU Health - Automatic Deployment's documentation!
=============================================================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   introduction
   overview
   playbooks/index
   roles/index
   examples/index
   vault
   ssh
   firewall
   backup
   upgrade
   tests
   roadmap
   contributing
   trouble
   license
