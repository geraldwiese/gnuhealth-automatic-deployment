#!/bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

bash tests/config_stock/check_config_stock.sh
bash tests/lint/lint_ansible.sh
bash tests/var_check/var_check.sh
reuse lint
