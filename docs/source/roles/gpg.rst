.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

gpg
===

This role installs GPG and creates a GPG key for the `ansible_user` running the `desktop` playbook.

The crypto plugin of GNU Health contains functionalities of digital signatures that can only be used with such a key.

- **Parameters**:

  - gpg_key_type: RSA or other key type
  - gpg_key_length: Bit length of the key
  - gpg_name: Name of key owner
  - gpg_mail: Email of key owner
  - gpg_pw: Password to protect the key
  - gpg_expiration_days: Number of days for validity of the key
  - gpg_create_subkey: Whether a subkey is needed (e.g. for elliptic curves)
  - gpg_subkey_type: Type of subkey
  - gpg_subkey_length: Bit length of subkey

- **Defaults**:


