.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

orthanc
=======

The DICOM server Orthanc is simply installed as a system package. Additionally all other `or-packages` set in configuration
are installed - for example webviewer and database extensions.

By default the internal HTTP server uses port 8042.

After the installation the DICOM server will be disabled because the HTTP server can overtake all its functionalities with encryption.
In order to be able to access the HTTP server from outside remote access will be allowed and authentication enabled.
Besides query results are limitied to 100 entries to avoid the server becoming unresponsive.
Thereby some proposed suggestions concerning the security are already realized.
Here you find some suggestions regarding the storage and the firewall as well:

https://book.orthanc-server.com/faq/security.html

If you want to allow overwriting instances set `overwrite_instances` to true.

Plugins for download can get selected, for example the outcommented ones are for the Stone Web Viewer and VolView.

Finally if `or_demo_users` is true three demo users are created with the following username:password pairs:

* admin:admin

* alice:bob

* bob:alice

For those demo users a LUA script will be copied in order to manage access permissions for the different HTTP methods.
The user admin has no restrictions, alice can only read / perform GET methods and bob can perform all methods except DELETE.
Of course those users have to be modified before making your system productive.

You find the configuration file and the LUA script in /etc/orthanc/.
The config files name is `orthanc.json` if using Debian/Ubuntu and `Configuration.json` if using openSUSE.
Credentials are set in credentials.json without being world-readable.
If `use_postgresql` is true the connection parameters are stored in postgresql.json which is not world-readable as well because the URI might contain credentials.

- **Parameters**:

  - or_overwrite_instances: If true allow to overwrite instances
  - or_demo_users: If true create demo users
  - or_allow_remote_access: If true allow access from outside (with authentication)
  - or_enable_dicom: If true enable DICOM
  - or_limit_find: Limit for cfind
  - or_deidentify_dicom: DICOM standard for deanonmyzation, see main config file for more information
  - or_packages: System packages to install
  - or_download_plugins: Plugins to download into `or_plugins_path`
  - or_use_postgresql: If true use PostgreSQL instead of SQLite
  - or_psql_uri: Connection string to connect to PostgreSQL
  - or_ssh_key: If true create SSH key for automated backups

- **Defaults**:

  - or_https: If true, serve HTTPS
  - or_cert_path: Path of certificate for Orthanc HTTPS
  - or_key_path: Path of key for Orthanc HTTPS
  - or_psql_uri_local: Default PostgreSQL URI for local access (UNIX socket)
  - or_psql_uri_remote: Default PostgreSQL URI for remote access (verify TLS)
  - or_psql_db_name: Name of PostgreSQL database used by Orthanc
  - or_psql_db_user: Name of PostgreSQL user who owns the database used by Orthanc
  - or_psql_domain: Domain name of PostgreSQL server
  - or_psql_pw: Password of PostgreSQL user
  - or_verify_psql: If true, verify PostgreSQL TLS
  - or_copy_ownca: If true, copy CA certificate for TLS verification
  - or_ownca_path: Path of CA certificate to trust
  - or_psql_cert_path: Path of server certificate to trust
  - or_package_version_pinning: If true use given package version
  - or_package_version: Package version if using version pinning
  - or_ssh_dir: Directory for `orthanc` users SSH, will be created if not present
  - or_ssh_key_path: Path of the private SSH key (public will be the same + '.pub')
  - or_ssh_key_remote_user: Remote user that is target of SSH connection for backups
  - or_default_packages: Default package selection based on OS family

- **Variables**:

  - or_config_path: Path of main config file based on OS family
  - or_config_dir: Path of config directory based on OS family
  - or_storage_path: Path of application data based on OS family
  - or_home: Path of OS users home directory based on OS family
  - or_plugins_path: Path where plugins are loaded from based on OS family
