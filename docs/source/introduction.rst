.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Introduction
============

The Ansible playbooks in this project aim to automatically deploy an hospital information system based on GNU Health and other free software.
The main elements are:

**HIS node**: Hospital Information System, core of the GNU Health system (role and playbook just called `gnuhealth`)

**Desktop**: Workstation with the GNU Health client to access the HIS node

**Orthanc**: The DICOM server is not directly part of the GNU Health system but its integration is provided

**Thalamus**: This is used to build up a GNU Health Federation which contains multiple HIS nodes (still in development)

Using the playbooks you can easily install the servers - even distributed on multiple systems.
Numerous configuration options are realized in order to ease and automize standard configuration steps.

Supported operating systems are the latest Debian, Ubuntu LTS, openSUSE Leap and FreeBSD.
