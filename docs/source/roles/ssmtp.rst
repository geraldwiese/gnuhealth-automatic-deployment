.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

ssmtp
=====

This role can be used to realize email notifications by using a dummy email account only for sending those emails.
If you want to use sSMTP, create a new mail account for this and make sure that access from applications is allowed.
It's tested using web.de which requires to "allow IMAP/POP3" although only SMTP will be used.
Installation of sSMTP is controlled by the boolean `ssmtp`. Besides it is not used if `postfix` is set to true.

The role comes with the following configuration options:

- **Parameters**:

  - ssmtp_mail_hub: Domain name and port of SMTP server of your mail provider
  - ssmtp_mail_address: Email address that is created for this purpose
  - ssmtp_mail_pw: Password to log into the mail address

- **Defaults**:

  - ssmtp_users: Users allowed to use sSMTP
  - ssmtp_zypper_repo: Link of zypper repository to add for installation on openSUSE Leap
  - ssmtp_template_comment: Comment put on top of every template delivered by Ansible
