# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
- name: Create directory for CSR
  ansible.builtin.file:
    path: "{{ cert_ssl_paths['csr_path'] }}"
    state: directory
    mode: '0755'
- name: Generate openssl private key
  community.crypto.openssl_privatekey:
    path: "{{ cert_ssl_paths['key_path'] }}{{ cert_vars.cert_params[-1] }}.key"
    type: RSA
    backup: true
  no_log: true
- name: Generate openssl certificate signing request
  community.crypto.openssl_csr:
    path: "{{ cert_ssl_paths['csr_path'] }}{{ cert_vars.cert_params[-1] }}.csr"
    privatekey_path: "{{ cert_ssl_paths['key_path'] }}{{ cert_vars.cert_params[-1] }}.key"
    country_name: "{{ cert_vars.cert_params[0] }}"
    state_or_province_name: "{{ cert_vars.cert_params[1] }}"
    locality_name: "{{ cert_vars.cert_params[2] }}"
    organization_name: "{{ cert_vars.cert_params[3] }}"
    organizational_unit_name: "{{ cert_vars.cert_params[4] }}"
    common_name: "{{ cert_vars.cert_params[5] }}"
    email_address: "{{ cert_vars.cert_admin_mail }}"
- name: Check if certificate exists
  ansible.builtin.stat:
    path: "{{ cert_ssl_paths['cert_path'] }}/{{ cert_vars.cert_params[-1] }}.crt"
  register: cert_server
- name: Get certificate from ownca on same system
  when:
    - cert_vars.renew or not cert_server.stat.exists
    - "not group_names | intersect(['application', 'nginx', 'postgresql']) or 'ownca' in group_names"
  block:
    - name: Generate openssl certificate (ownca same system)
      ansible.builtin.command: >
        openssl ca -config {{ cert_ownca_params.openssl_config }} -in {{ cert_ssl_paths.csr_path }}{{ cert_vars.cert_params[-1] }}.csr
        -batch -out {{ cert_ssl_paths['cert_path'] }}{{ cert_vars.cert_params[-1] }}.crt
      changed_when: true
    - name: Get ownca content
      ansible.builtin.command: "cat {{ cert_ownca_params.dir }}/{{ cert_ownca.cert_params[-1] }}.crt"
      register: cert_ownca_content
      changed_when: false
    - name: Append ownca to cert (create chain)
      ansible.builtin.blockinfile:
        path: "{{ cert_ssl_paths['cert_path'] }}{{ cert_vars.cert_params[-1] }}.crt"
        block: "{{ cert_ownca_content.stdout }}"
- name: Get certificate from separate ownca system
  when:
    - cert_vars.renew or not cert_server.stat.exists
    - "group_names | intersect(['application', 'nginx', 'postgresql']) and not 'ownca' in group_names"
  block:
    - name: Fetch certificate signing request
      ansible.builtin.fetch:
        src: "{{ cert_ssl_paths['csr_path'] }}{{ cert_vars.cert_params[-1] }}.csr"
        dest: "../fetch/"
        flat: true
    - name: Copy certificate signing request
      ansible.builtin.copy:
        src: "../fetch/{{ cert_vars.cert_params[-1] }}.csr"
        dest: "{{ cert_ownca_params.dir }}/csr/{{ cert_vars.cert_params[-1] }}.csr"
        mode: '0664'
      delegate_to: "{{ groups['ownca'][0] }}"
    - name: Generate openssl certificate
      ansible.builtin.command: >
        openssl ca -config {{ cert_ownca_params.openssl_config }} -in {{ cert_ownca_params.dir }}/csr/{{ cert_vars.cert_params[-1] }}.csr
        -batch -out {{ cert_ssl_paths['cert_path'] }}{{ cert_vars.cert_params[-1] }}.crt
      delegate_to: "{{ groups['ownca'][0] }}"
      changed_when: true
    - name: Get ownca content
      ansible.builtin.command: "cat {{ cert_ownca_params.dir }}/{{ cert_ownca.cert_params[-1] }}.crt"
      register: cert_ownca_content
      delegate_to: "{{ groups['ownca'][0] }}"
      changed_when: false
    - name: Append ownca to cert (create chain)
      ansible.builtin.blockinfile:
        path: "{{ cert_ssl_paths['cert_path'] }}{{ cert_vars.cert_params[-1] }}.crt"
        block: "{{ cert_ownca_content.stdout }}"
      delegate_to: "{{ groups['ownca'][0] }}"
    - name: Fetch certificate
      ansible.builtin.fetch:
        src: "{{ cert_ssl_paths['cert_path'] }}{{ cert_vars.cert_params[-1] }}.crt"
        dest: "../fetch/"
        flat: true
      delegate_to: "{{ groups['ownca'][0] }}"
    - name: Copy certificate to target system
      ansible.builtin.copy:
        src: "../fetch/{{ cert_vars.cert_params[-1] }}.crt"
        dest: "{{ cert_ssl_paths['cert_path'] }}"
        backup: true
        mode: '0644'
- name: Get cert and key filenames for further use
  ansible.builtin.set_fact:
    cert: '{{ cert_vars.cert_params[-1] }}.crt'
    key: '{{ cert_vars.cert_params[-1] }}.key'
