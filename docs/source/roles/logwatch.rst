.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

logwatch
========

This role installs Logwatch in order to ship log files. It keeps standard configuration options and enables to specify global detail and debug levels.
Besides you can create simple custom filters for non-supported services. However those are not able to filter the logfiles by date (usually old entries are skipped).
The installation on Debian implies a Cron job that sends a nightly report.

The role comes with the following configuration options:

- **Parameters**:

  - lw_detail: Global option to set how much details of log files should be sent
  - lw_debug: Debug level
  - lw_custom_filters: Define custom filters for shipping log files of services logwatch does not support yet
  - lw_ssmtp: If true, use `lw_mailto` and `lw_mailfrom` to specify email sender and recipient
  - lw_mailto: Email recipient (ssmtp only)
  - lw_mailfrom: Email sender (ssmtp only)

- **Defaults**:

  - lw_copy_default_config: Copy default config to main config path if true (does not overwrite if it exists already)
  - lw_filter_only_if_service_exists: Only create filters in `lw_custom_filters` if a service with its name exists

- **Variables**:

  - lw_cache_dir: Cache directory for Logwatch, has to be created
  - lw_main_config_path: Main config files path
  - lw_default_config_path: Path of default main config file to copy
  - lw_override_config_path: Path for drop-in config file to override
  - lw_logfile_dir: Directory for logfile configuration, used for custom filters
  - lw_service_dir: Directory for service configuration, used for custom filters
  - lw_script_dir: Directory for script executables, used for custom filters
