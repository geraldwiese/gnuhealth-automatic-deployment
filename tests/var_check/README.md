<!--
SPDX-FileCopyrightText: 2023 Jonathan Feilmeier

SPDX-License-Identifier: GPL-3.0-or-later
-->

# var_check
Contains scripts and files to run consistency and documentation checks for declerated variables

## var_check.sh
* Runs consistency checks of gnuhealth, orthanc and thalamus groupvars
* Runs consistency checks for roles against gnuhealth, orthanc and thalamus role
* Runs documentation checks for variables used in roles

## var_check_delimiter.py
Script to check consistency of vars between delimiters or given by .yml file
See python3 var_check_delimiter.py --help to get usage instructions or have a look at usage in var_check.sh

## doc_check.py
Script to check if variables are documented
See python3 doc_check.py --help to get usage instructions or have a look at usage in var_check.sh

## gnuhealth_keys_vars.yml
Keys to check for groupvars consistency of vars.yml files

## gnuhealth_keys_vault.yml
Keys to check for groupvars consistency of vault.yml files

## omit_keys.yml
Keys to omit during consisteny checks