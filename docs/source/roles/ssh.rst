.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

ssh
===

The SSH role can be used to create and trust SSH keys. This can be useful for the very first steps on a server but it is also used in playbooks for a backup server to make sure that the users of applications can connect and push backups.

Keys can be trusted either using filepaths or strings directly. Host keys can also get added to `known_hosts` because otherwise the first connection would not work non-interactively.

The port of the SSH daemon can also be configured and password based access can get disabled.

Every task needs a boolean or list element in order to actually perform change. By default the role would change nothing because SSH configuration is dangereous to lock someone out of their server.

The following variables control what happens:

- **Parameters**:

  - ssh_username: Name of the user for establishing a SSH connection in the playbook
  - ssh_key_create: If true create a SSH key
  - ssh_key_user: User that gets a new SSH key
  - ssh_key_dir: Directory for the key, will be created if not present
  - ssh_key_path: Path of the new private key, public will be + '.pub'
  - ssh_key_fetch: If true fetch the public key
  - ssh_key_fetch_path: Path where to save a fetched public SSH key
  - ssh_key_trust_local_paths: Trust keys from files of given path for given user
  - ssh_key_trust_strings: Trust keys from given strings for given user
  - ssh_trust_host: If true add host keys to `known_hosts`
  - ssh_host_to_trust: Hostname of server to add to `known_hosts`
  - ssh_host_to_trust_port: SSH port of host to trust
  - ssh_known_host_user: User that trusts host keys
  - ssh_set_port: If true set the port of the SSH service in override config file
  - ssh_port: TCP port of the SSH service
  - ssh_disable_pw_auth: If true, disable password based access in override config file
  - ssh_set_log_level: If true set the log level of the SSH service in override config file
  - ssh_log_level: The log level of SSH. See `man sshd_config` for available options. Note that `Debug` is needed if Fail2Ban should ban IP addresses after failed login attempts using SSH keys. On the other hand the man page states that `Logging with a DEBUG level violates the privacy of users and is not recommended`.
  - ssh_dropin_path: Path where to put the dropin config file for overriding default variables. Default path is based on the Debian path which is set by its openssh-server package distributed by apt for the directory and begins with `00` for the filename as SSH takes the value it finds first (see `man sshd_config` for more information).
  - ssh_args: Can be set to disable host key checking at first connection or any other args
  - ssh_template_comment: Comment put on top of every template delivered by Ansible

