#! /bin/bash

# SPDX-FileCopyrightText: 2023 Jonathan Feilmeier
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

bailout() {
    printf "\033[31mError encountered: Check if the files were found and if the commands showed errors or inequalities\033[0m\n"
    exit 1
}

echo "Check group_vars for equality"
python3 tests/var_check/var_check_delimiter.py inventories/dev/group_vars/gnuhealth/vars.yml inventories/dev/group_vars/orthanc/vars.yml -o tests/var_check/omit_keys.yml -k tests/var_check/gnuhealth_keys_vars.yml || bailout
python3 tests/var_check/var_check_delimiter.py inventories/dev/group_vars/gnuhealth/vault.yml inventories/dev/group_vars/orthanc/vault.yml -o tests/var_check/omit_keys.yml -k tests/var_check/gnuhealth_keys_vault.yml || bailout
python3 tests/var_check/var_check_delimiter.py inventories/dev/group_vars/gnuhealth/vars.yml inventories/dev/group_vars/thalamus/vars.yml -o tests/var_check/omit_keys.yml -k tests/var_check/gnuhealth_keys_vars.yml || bailout
python3 tests/var_check/var_check_delimiter.py inventories/dev/group_vars/gnuhealth/vault.yml inventories/dev/group_vars/thalamus/vault.yml -o tests/var_check/omit_keys.yml -k tests/var_check/gnuhealth_keys_vault.yml || bailout


echo "Check consistency between GNU Health inventory and role defaults\n\n\n"
for role in $(ls -I check -I test-server -I gnuhealth-client -I gpg -I mygnuhealth -I orthanc -I ssh -I thalamus roles); do
    python3 tests/var_check/var_check_delimiter.py roles/$role/defaults/main.yml inventories/dev/group_vars/gnuhealth/vars.yml -o tests/var_check/omit_keys.yml|| bailout
done

echo "Check consistency between Thalamus inventory and role defaults\n\n\n"
for role in $(ls -I check -I test-server -I gnuhealth -I gnuhealth-client -I gpg -I mygnuhealth -I orthanc -I restic -I ssh roles); do
    python3 tests/var_check/var_check_delimiter.py roles/$role/defaults/main.yml inventories/dev/group_vars/thalamus/vars.yml -o tests/var_check/omit_keys.yml|| bailout
done

echo "Check consistency between Orthanc inventory and role defaults\n\n\n"
for role in $(ls -I check -I test-server -I gnuhealth -I gnuhealth-client -I gpg -I mygnuhealth -I restic -I ssh -I thalamus -I uwsgi roles); do
    python3 tests/var_check/var_check_delimiter.py roles/$role/defaults/main.yml inventories/dev/group_vars/orthanc/vars.yml -o tests/var_check/omit_keys.yml|| bailout
done

printf "\033[32mVariables in inventories are up to date\033[0m\n\n\n"

echo "Check for existing documentation of the roles variables\n\n\n"
for role in $(ls roles); do
    [ ! -d "./roles/$role/vars" ] && printf "\033[33mWarning: Directory roles/%s/vars does not exist.\nContinue without checking directory.\n\n\033[0m" $role && continue
    python3 tests/var_check/doc_check.py roles/$role/vars/main.yml docs/source/roles/$role.rst --section Variables || bailout
done

for role in $(ls roles); do
    [ ! -d "./roles/$role/defaults" ] && printf "\033[33mWarning: Directory roles/%s/defaults does not exist.\nContinue without checking directory.\n\n\033[0m" $role && continue
    python3 tests/var_check/doc_check.py roles/$role/defaults/main.yml docs/source/roles/$role.rst --section Defaults Parameters || bailout
done
