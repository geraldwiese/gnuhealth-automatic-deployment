#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

echo "Setting config values"
export SSH_USER_TUTORIAL='vagrant'
export SSH_PW_TUTORIAL='vagrant'
export SSH_PORT_TUTORIAL=63671
export MAIL_PW='gnusolidario'
export MAIL_SOURCE='source@example.com'
export MAIL_HUB='smtp.web.de:465'
export MAIL_TARGET='target@example.com'
export LCL='de_DE'
export DN_NGINX='server'
export TIMEZONE='Europe/Berlin'
export NFT_INTERFACES='{eth0,eth1}'
export NFT_SUBNET="192.168.60.0/24"
export IP_SERVER="192.168.60.110"
export IP_BACKUP="192.168.60.111"
export DT_PROFILE_NAME="Tutorial"

echo "Setting paths"
export NEW_INVENTORY_PATH=inventories/test/gnuhealth_prod_small
export SSH_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/ssh/vars.yml
export GH_VAULT_PATH=$NEW_INVENTORY_PATH/group_vars/gnuhealth/vault.yml
export GH_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/gnuhealth/vars.yml
export DT_VAULT_PATH=$NEW_INVENTORY_PATH/group_vars/desktop/vault.yml
export DT_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/desktop/vars.yml

echo "Recreating test inventory from dev"
[ ! -d $NEW_INVENTORY_PATH ] || rm -r $NEW_INVENTORY_PATH
cp -r inventories/dev $NEW_INVENTORY_PATH

echo "Rewriting hosts file"
rm $NEW_INVENTORY_PATH/hosts
cat > $NEW_INVENTORY_PATH/hosts <<EOL
[gnuhealth]

[gnuhealth:children]
gnuhealth_app
gnuhealth_psql
gnuhealth_nginx
gnuhealth_ownca
gnuhealth_bak

[gnuhealth_app]
server ansible_port=22

[gnuhealth_psql]
server ansible_port=22

[gnuhealth_nginx]
server ansible_port=22

[gnuhealth_ownca]
server ansible_port=22

[gnuhealth_bak]
backup ansible_port=22


[desktop]
client ansible_port=22


[application:children]
gnuhealth_app

[postgresql:children]
gnuhealth_psql

[nginx:children]
gnuhealth_nginx

[ownca:children]
gnuhealth_ownca

[bak:children]
gnuhealth_bak


[ssh]

[ssh:children]
gnuhealth
desktop

EOL

echo "Adjusting SSH configuration"
sed -i "s?^.*ssh_username:.*?ssh_username:\ ${SSH_USER_TUTORIAL}?" $SSH_VARS_PATH
sed -i "s/ssh_key_trust_local_paths\:\ \[\]/ssh_key_trust_local_paths\:/g" $SSH_VARS_PATH
sed -i "s?^.*-\ key:\ /path/.*?\ \ -\ key:\ ../fetch/controller.pub?" $SSH_VARS_PATH
grep -qxF 'user: vagrant' $SSH_VARS_PATH || sed -i "/controller.pub/a \ \ \ \ user:\ vagrant" $SSH_VARS_PATH
sed -i "s?^.*ssh_set_port:.*?ssh_set_port:\ true?" $SSH_VARS_PATH
sed -i "s?^.*ssh_port:.*?ssh_port:\ 63671?" $SSH_VARS_PATH
sed -i "s?^.*ssh_disable_pw_auth:.*?ssh_disable_pw_auth:\ true?" $SSH_VARS_PATH
sed -i "s?^.*ssh_set_log_level:.*?ssh_set_log_level:\ true?" $SSH_VARS_PATH

echo "Adjusting gnuhealth/vault.yml"
sed -i "s?^.*vault_ssh_user:.*?vault_ssh_user:\ ${SSH_USER_TUTORIAL}?" $GH_VAULT_PATH
sed -i "s?^.*vault_set_sudo_pass:.*?vault_set_sudo_pass:\ true?" $GH_VAULT_PATH
sed -i "s?^.*vault_ssh_password:.*?vault_ssh_password:\ ${SSH_USER_TUTORIAL}?" $GH_VAULT_PATH
sed -i "s?^.*vault_mail_password:.*?vault_mail_password:\ ${MAIL_PW}?" $GH_VAULT_PATH
for var in "vault_psql_pw" "vault_psql_pw_barman" "vault_tryton_pw" "vault_rstc_pw" "vault_gh_bak_password"; do
  sed -i "s?^.*${var}:.*?${var}:\ '$(openssl rand -base64 20)'?" $GH_VAULT_PATH
done

echo "Adjusting gnuhealth/vars.yml"
sed -i "s?^.*gh_tryton_admin_mail:.*?gh_tryton_admin_mail:\ '${MAIL_TARGET}'?" $GH_VARS_PATH
sed -i "s?^.*psql_set_encoding:.*?psql_set_encoding:\ true?" $GH_VARS_PATH
sed -i "s?^.*psql_set_locale:.*?psql_set_locale:\ true?" $GH_VARS_PATH
sed -i "s?^.*psql_locale:.*?psql_locale:\ '${LCL}'?" $GH_VARS_PATH
sed -i "s?^.*psql_barman_host_port:.*?psql_barman_host_port:\ ${SSH_PORT_TUTORIAL}?" $GH_VARS_PATH
export HBA_LINE="\ \ -\ \"hostssl\treplication\tstreaming_barman\t${IP_BACKUP}/32\tscram-sha-256\""
grep -qxF "${HBA_LINE}" $GH_VARS_PATH || sed -i "/psql_hba_rules:/a ${HBA_LINE}" $GH_VARS_PATH
export HBA_LINE="\ \ -\ \"hostssl\tall\tbarman\t${IP_BACKUP}/32\tscram-sha-256\""
grep -qxF "${HBA_LINE}" $GH_VARS_PATH || sed -i "/psql_hba_rules:/a ${HBA_LINE}" $GH_VARS_PATH
sed -i "s?^.*nginx_rproxy_domain:.*?nginx_rproxy_domain:\ ${DN_NGINX}?" $GH_VARS_PATH
sed -i "s?^.*nginx_https:.*?nginx_https:\ true?" $GH_VARS_PATH
sed -i "s?^.*nginx_http:.*?nginx_http:\ false?" $GH_VARS_PATH
sed -i "s?^backup:.*?backup:\ true?" $GH_VARS_PATH
sed -i "s?^.*brm_ssh_port:.*?brm_ssh_port:\ ${SSH_PORT_TUTORIAL}?" $GH_VARS_PATH
sed -i "s?^.*brm_ssh_port_psql:.*?brm_ssh_port_psql:\ ${SSH_PORT_TUTORIAL}?" $GH_VARS_PATH
sed -i "s?:22/restic-repo?:${SSH_PORT_TUTORIAL}/restic-repo?g" $GH_VARS_PATH
sed -i "s?^locale:.*?locale:\ true?" $GH_VARS_PATH
sed -i "s?de_DE?${LCL}?g" $GH_VARS_PATH
sed -i "s?^.*fail2ban:.*?fail2ban:\ true?" $GH_VARS_PATH
sed -i "s?^.*f2b_nginx:.*?f2b_nginx:\ \"{{\ inventory_hostname\ in\ groups.nginx\ }}\"?" $GH_VARS_PATH
sed -i "s?^ssmtp:.*?ssmtp:\ true?" $GH_VARS_PATH
sed -i "s?^.*ssmtp_mail_hub:.*?ssmtp_mail_hub:\ \"${MAIL_HUB}\"?" $GH_VARS_PATH
sed -i "s?^.*ssmtp_mail_address:.*?ssmtp_mail_address:\ ${MAIL_SOURCE}?" $GH_VARS_PATH
sed -i "s?^.*logwatch:.*?logwatch:\ true?" $GH_VARS_PATH
sed -i "s?^.*lw_mailto:.*?lw_mailto:\ ${MAIL_TARGET}?" $GH_VARS_PATH
sed -i "s?^.*systemd_alert:.*?systemd_alert:\ true?" $GH_VARS_PATH
sed -i "s?^.*sa_admin_mail:.*?sa_admin_mail:\ ${MAIL_TARGET}?" $GH_VARS_PATH
sed -i "s?^timezone:.*?timezone:\ true?" $GH_VARS_PATH
sed -i "s?^.*nftables:.*?nftables:\ true?" $GH_VARS_PATH
sed -i "s?^.*nft_network_interface:.*?nft_network_interface:\ '${NFT_INTERFACES}'?" $GH_VARS_PATH
sed -i "s?^.*nft_internal_subnet:.*?nft_internal_subnet:\ '${NFT_SUBNET}'?" $GH_VARS_PATH
sed -i "s/dport {80, 443}/dport 443/g" $GH_VARS_PATH
sed -i "s/dport 22/dport ${SSH_PORT_TUTORIAL}/g" $GH_VARS_PATH
export NFT_LINE="\ \ -\ \"iif {{ nft_network_interface }} tcp dport 5432 ip saddr ${IP_BACKUP}/32 accept\""
grep -qxF "${NFT_LINE}" $GH_VARS_PATH || sed -i "/nft_allow_input_lines:/a ${NFT_LINE}" $GH_VARS_PATH
export NFT_LINE="\ \ -\ \"iif {{ nft_network_interface }} tcp dport 5432 ip saddr ${IP_SERVER}/32 accept\""
grep -qxF "${NFT_LINE}" $GH_VARS_PATH || sed -i "/nft_allow_input_lines:/a ${NFT_LINE}" $GH_VARS_PATH

echo "Adjusting desktop/vault.yml"
sed -i "s?^.*vault_ssh_user:.*?vault_ssh_user:\ ${SSH_USER_TUTORIAL}?" $DT_VAULT_PATH
sed -i "s?^.*vault_set_sudo_pass:.*?vault_set_sudo_pass:\ true?" $DT_VAULT_PATH
sed -i "s?^.*vault_ssh_password:.*?vault_ssh_password:\ ${SSH_USER_TUTORIAL}?" $DT_VAULT_PATH

echo "Adjusting desktop/vars.yml"
sed -i "s?^.*ghcl_profile_name:.*?ghcl_profile_name:\ ${DT_PROFILE_NAME}?" $DT_VARS_PATH
sed -i "s?^.*ghcl_trust_certs:.*?ghcl_trust_certs:\ true?" $DT_VARS_PATH
sed -i "s?^timezone:.*?timezone:\ true?" $DT_VARS_PATH
sed -i "s?^.*tz_timezone:.*?tz_timezone:\ ${TIMEZONE}?" $DT_VARS_PATH

echo "Finished generating inventory"

