.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

gnuhealth
=========

This role installs the application for the HIS using PyPI. It is not intended to use the builtin `werkzeug` HTTP server but to get execeuted using uwSGI.
It is the only role yet that has its separate repository and can also be integrated in other Ansible setups:

https://gitlab.com/geraldwiese/gnu-health-ansible-role


Main Installation
-----------------

In the beginning some system packages are installed that are needed later on.
Then a new operating system user `gnuhealth` without login shell and SSH access is created.
By default the following directories are created:

- `/opt/gnuhealth` -- home folder

- `/opt/gnuhealth/etc` -- config folder

- `/opt/gnuhealth/var/log` -- log folder

- `/opt/gnuhealth/var/lib` -- data folder

- `/opt/gnuhealth/var/run` -- run folder

- `/opt/gnuhealth/tls` -- TLS folder

- `/opt/gnuhealth/venv` -- virtual python environment

The run folder contains the UNIX socket if GNU Health & Nginx are on the same system.

In case of separate systems a certificate and key will be stored in the TLS folder for enabling HTTPS.
The certificate and CRL of PostgreSQL will also be transferred in this directory for verification.

GNU Health itself is installed using the PyPI package `gnuhealth-all-modules` inside the virtual environment.
If using openSUSE Python 3.9 will be installed and used for it in order not to use Python 3.6 which has reached end of life.

After the installation the database created by PostgreSQL before will be initialized using Tryton.
Besides the modules specified in `gh_modules` are activated and countries and currencies will get imported.

Configuration options for GNU Health itself are prefixed by `gh_`. Templates in use can be found in `roles/gnuhealth/templates`.
The `trytond.conf` is based on the one shipped by zypper on openSUSE.

The application will be executed using uWSGI (see its role).


Additional steps
----------------

Optionally you can also add the demo database to your system by setting `gh_demo_db` to true.

For using the calendar set `gh_webdav` true as well.

If Trytons builtin Cron should be active, you can enable `gh_cron`.

The modules are all installed but by default only the core module `health` is activated in the database.
You can also extend the list `gh_modules`. Note that you will also get the depending ones activated.

If you let the playbooks install Orthanc using a custom CA and want to connect Orthanc to the HIS you can trust this CA by setting `gh_trust_cert_orthanc` to true and specifying the path.

In the past GNU Health already shipped some aliases for easing the process of remote support.
Those are mainly overtaken and some modified.
They are shipped as .gnuhealthrc which is now activated by the .bashrc of the `gnuhealth` user:

- `activate`: Activates the virtual python environment (makes former cdexe obsolete)

- `cdlogs`: Change to the directory where log files are stored

- `cdconf`: Change to the config directory

- `cdmods`: Change to the directory containing GNU Health modules

- `editconf`: Edit the main config file trytond.conf

The traditional gnuhealth-control command is also shipped in a way adapted to the rest of the installation. It can be used for backups on local devices or updates.
However it is strongly recommended to use separate systems for automated backups and therefor the barman & restic role can be used.

If you set `gh_update_weekly` to true, a BASH script is shipped that will get triggered weekly by Cron (on OS level, not Tryton) to run minor updates on GNU Health using pip.
Additionally `gh_restart_service_after_update` can be set to true to trigger a service restart after an update.
In a test environment uWSGI was able to restart its service without a break of accessibility (having 10 connectivity tests per second).
Nevertheless this should not be enabled for productive systems without further individual tests.


Other installation methods
--------------------------

If you want to compare this to other installation strategies have a look at Wikibooks

https://en.wikibooks.org/wiki/GNU_Health/Installation

or openSUSE

https://en.opensuse.org/GNUHealth_on_openSUSE

Variables
---------

- **Parameters**:

  - gh_version: The version of GNU Health HIS on PyPI as `gnuhealth-all-modules`
  - gh_psql_uri: The connection string for connecting to the PostgreSQL server
  - gh_demo_db: Whether a local demo database should be installed
  - gh_webdav: Whether a service for WebDAV should be created
  - gh_cron: Whether Tryton Cron should run
  - gh_tryton_admin_mail: An admin email address visible for Tryton
  - gh_modules: GNU Health/Tryton modules to activate
  - gh_update_weekly: Whether weekly PyPI updates should run
  - gh_restart_service_after_update: Whether the systemd service should restart after updates
  - gh_trust_cert_orthanc: Whether a certificate of Orthanc should be trusted
  - gh_trust_certs: Path(s) of the certificate to trust
  - gh_tryton_pw: Admin password for GNU Health/Tryton client
  - gh_ssh_key: Whether an SSH key should be created for the `gnuhealth` user

- **Defaults**:

  - gh_home_path: Home directory of `gnuhealth` user
  - gh_config_path: Directory for config files
  - gh_log_path: Directory for log files
  - gh_data_path: Directory for attachments
  - gh_run_path: Run directory
  - gh_tls_path: TLS directory
  - gh_venv: Virtual environments path
  - gh_rc_path: Path to put the gnuhealthrc file containing environment variables and aliases, meant to be loaded from .bashrc and for service execution
  - gh_psql_uri_local: PostgreSQL URI for local connection (UNIX socket)
  - gh_psql_uri_remote: PostgreSQL URI for remote connection (verify TLS)
  - gh_prepare_client: Whether a vars file should be prepare for the client installation to overtake connection information
  - gh_webdav_port: Port of the WebDAV service
  - gh_install_libreoffice: Install LibreOffice if true
  - gh_install_libreoffice_recommends: Install recommended packages as well when installing LibreOffice
  - gh_https: Prepare HTTPS for uWSGI if true
  - gh_cert: `src` and `dest` for copying certificate
  - gh_key: `src` and `dest` for copying key
  - gh_psql_verify_tls: If PostgreSQL TLS should get verified
  - gh_psql_cert_path: Certificate for verifying PostgreSQL TLS
  - gh_psql_cert_path_ownca: Default path to pass to `gh_psql_cert_path` when using custom CA
  - gh_psql_cert_path_remote: Default path to pass to `gh_psql_cert_path` when using existing certificate already on server
  - gh_psql_cert_path_local: Default path to pass to `gh_psql_cert_path` when using existing certificate from Ansible controller
  - gh_psql_use_crl: Whether GNU Health should hold a Certificate Revocation List (CRL) for PostgreSQL in case of custom CA
  - gh_psql_crl_path: Path of the CRL
  - gh_psql_db_name: PostgreSQL database name used for GNU Health
  - gh_psql_db_user: PostgreSQL user who is owner of the database
  - gh_psql_domain: Domain name of PostgreSQL server
  - gh_psql_use_pw: Whether PostgreSQL user has a password
  - gh_psql_pw: Password of the PostgreSQL user
  - gh_ssh_dir: Directory for `gnuhealth` users SSH, will be created if not present
  - gh_ssh_key_path: Path of the private SSH key (public will be the same + '.pub')
  - gh_ssh_key_remote_user: Remote user that is target of SSH connection for backups
  - gh_uwsgi_unix_socket: Whether uWSGI should spawn a UNIX socket (otherwise TCP and either uwsgi protocol or HTTPS if `gh_https` is true)
  - gh_uwsgi_instance: Dictionary containing all the parameters passed to uWSGI role
  - gh_template_comment: Comment put on top of every template delivered by Ansible
  - gh_module_list: List of all available modules that can possibly get added to `gh_modules`

- **Variables**:

  - gh_use_testpypi: Whether TestPyPI should be used instead of PyPI, this is used for testing the `gnuhealth-all-modules` package before publishing
  - gh_demo_db_version: Version of the demo database
  - gh_demo_db_name: Name of the demo database
  - gh_packages: Packages to be installed on the system differentiated by OS family
  - gh_libreoffice_packages: Package names of LibreOffice Writer & Calc differentiated by OS family, preferring `no-gui` for Debian
  - gh_py_suse_packages: Packages to install for having a current Python version on openSUSE Leap
  - gh_py_suse_exe: Executable of newer Python on openSUSE Leap
  - gh_py_suse_upgrade: Boolean to control whether `gh_py_suse_packages` should be installed (for the future, setting it false would break it currently)
  - gh_become_method_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - gh_become_exe_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - gh_become_flags_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
