.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

nftables
========

This role installs nftables, applies a config template and restarts the service if something changed.
The template is kept simple and does set the policy for outgoing and forwarded traffic to accept but regulates the input chain: 
Traffic from the loopback interface, ICMP (pings) and responses to outgoing traffic are allowed in the template.
Furthermore traffic that matches the configurable lines is allowed but otherwise the policy is drop.

- **Parameters**:

  - nft_network_interface: Network interface(s) used in lines for template
  - nft_internal_subnet: Subnet IP range used in lines for template
  - nft_allow_input_lines: Lines to put into template in order to allow specific network traffic

- **Defaults**:

  - nft_template_comment: Comment put on top of every template delivered by Ansible
