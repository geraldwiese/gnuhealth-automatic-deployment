.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

One application - multiple systems
==================================

This example automates the deployment of a service split into multiple systems.
Inside the `vagrant` folder there are Vagrantfiles for different scenarios.
Those are not only running Ansible but also creating the VMs using VirtualBox.
Choosing the subfolder `gnuhealth_split` spawns GNU Health split into three Debian servers and one Ubuntu desktop system each having 1GB RAM.
The following installation procedure for the host was only tested on Ubuntu yet but should be similiar for other GNU/Linux distributions:

.. code-block:: console

  $ sudo apt install pipx mercurial virtualbox sshpass wget
  $ wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  $ echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
  $ sudo apt update && sudo apt install vagrant
  $ pipx ensurepath
  $ source ~/.bashrc
  $ pipx install ansible-core
  $ ansible-galaxy collection install community.crypto community.general community.postgresql ansible.posix
  $ hg clone https://hg.savannah.gnu.org/hgweb/health-ansible/
  $ cd health-ansible/vagrant/gnuhealth_split
  $ vagrant up

This way you can easily test configurations used for separated systems. You will get four systems called `db`, `app`, `web` and `gui`.
If you want to connect to those VMs you can either use VirtualBox and click on `show`.
Or if you want to connect e.g. to the application GNU Health itself you can execute `vagrant ssh app` from the terminal in the same directory as above.

As you can see in the `Vagrantfile` in the folder you `cd` into there are only few configuration options set to change the default ones.
If you want to imitate the scenario without Vagrant & VirtualBox locate the lines setting `ansible.extra_vars` and check the `hosts` file.
Besides you can modify the choice of operating system, playbook, SSH port & IP addresses in the `Vagrantfile`.
