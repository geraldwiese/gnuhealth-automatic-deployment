# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

from proteus import config
print('Connecting to the server using HTTP...')
conf = config.set_xmlrpc("http://admin:gnusolidario@localhost/health/")
print('Success')
