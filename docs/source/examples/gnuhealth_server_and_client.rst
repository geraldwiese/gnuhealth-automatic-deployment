.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

GNU Health server and client in one system
==========================================

This example is meant to be followed in an existing Virtual Machine (VM).
It installs a GNU Health server and client on one system, running Ansible locally on the same system.
This scenario is intended to be as easy as possible to create an instance for testing or developing.

`Note for users without GNU/Linux experience: The following commands are meant to be executed in the terminal.
Often you can open it using Ctrl+Alt+T. Copy & Paste inside a terminal has the shortcuts Ctrl+Shift+C & Ctrl+Shift+V, don't forget the Shift.
If using VirtualBox you should either open this instructions inside the VM or install Guest Additions for a shared clipboard.`

Debian
------

The user performing the deployment needs sudo access.
If you did not grant sudo access yet, run the following commands:

.. code-block:: console

    $ su -
    # usermod -aG sudo <username>
    # exit

Afterwards log out and log in again. Use the command `groups` to verify if it worked.
Now follow the instructions for Ubuntu.

Ubuntu
------

* Install the requirements:

.. code-block:: console

    $ sudo apt install pipx mercurial
    $ pipx ensurepath
    $ source ~/.bashrc
    $ pipx install ansible-core
    $ ansible-galaxy collection install community.crypto community.general community.postgresql ansible.posix

* Clone and enter this repository:

.. code-block:: console

    $ hg clone https://hg.savannah.gnu.org/hgweb/health-ansible/
    $ cd health-ansible/

* Run the playbook:

.. code-block:: console

    $ ansible-playbook playbooks/gnuhealth_minimal.yml -i inventories/dev -c local -e ansible_user=`whoami` -K

You will get a prompt for the sudo/become password thanks to the -K flag.

* You can run the client either using the desktop entry or from terminal:

.. code-block:: console

    $ gnuhealth-client

* Hit `Connect` and enter the password `gnusolidario`.

openSUSE Leap
-------------

* Install the requirements:

.. code-block:: console

    $ sudo zypper install python3-pip python39 mercurial
    $ python3.9 -m venv venv
    $ source venv/bin/activate
    $ pip3 install ansible-core
    $ ansible-galaxy collection install community.crypto community.general community.postgresql ansible.posix

* Follow the rest of the instructions for Ubuntu (starting with 'hg clone ...')

FreeBSD
-------

This was tested with a FreeBSD 14.0 VM where UFS & MBR where chosen and the user was added to the `wheel` group during the installation.

* Preparations as root:

.. code-block:: console

    $ su
    # mount -o acls /
    # pkg update
    # pkg install git py39-pipx rust sudo bash
    # ln -s /usr/local/bin/python3.9 /usr/bin/python3
    # visudo
       → Uncomment or add line '%wheel ALL=(ALL:ALL) ALL'
       → Save and close by hitting 'Esc', ':wq' and 'Enter'
    # exit

* Install Ansible:

.. code-block:: console

    $ echo “export PATH=$PATH:/home/`whoami`/.local/bin” >> ~/.shrc
    $ . ~/.shrc
    $ pipx install ansible-core
    $ ansible-galaxy collection install community.crypto community.general community.postgresql ansible.posix

* Follow the rest of the instructions for Ubuntu (starting with 'hg clone ...')
