.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Examples
========
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gnuhealth_server_and_client
   gnuhealth_client
   one_application_multiple_systems
   one_system_multiple_applications
   gnuhealth_prod_small
