.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

thalamus
========

This playbook works like `gnuhealth` as well - it also uses uWSGI.

Check the Thalamus role for specific information.
