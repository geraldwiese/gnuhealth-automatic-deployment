.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

One system - multiple applications
==================================

This scenario is meant to use a single VM for installing GNU Health HIS, Orthanc and Thalamus alongside for testing or developing its interaction.

First of all follow the first example including the step to clone and enter this repository but do not execute any playbook.

Next create new folders and YAML files in the inventory folder:

.. code-block:: console

	$ mkdir inventories/dev/group_vars/{application,nginx,postgresql}
	$ touch inventories/dev/group_vars/{application,nginx,postgresql}/vars.yml

Then open those files in a text editor and fill them with the following content where some was identical anyway and some lists of different services are merged.

For uWSGI we only merge `uwsgi_instances` in application/vars.yml.
Additionally we specify the names of users and databases that the different applications use for PostgreSQL to avoid that they get mixed up:

::

	uwsgi_instances:
	  - "{{ th_uwsgi_instance }}"
	  - "{{ gh_uwsgi_instance }}"
	gh_psql_db_user: 'gnuhealth'
	gh_psql_db_name: 'health'
	th_psql_db_user: 'thalamus'
	th_psql_db_name: 'federation'
	or_psql_db_user: 'orthanc'
	or_psql_db_name: 'orthancdb'

Next we merge `nginx_sites_http` and change its ports in nginx/vars.yml:

::

	nginx: true
	nginx_rproxy_domain: "{{ groups['nginx'][0] | default('localhost') }}"  # for certificate & reverse proxy
	nginx_application_domain: "{{ groups['application'][0] | default('localhost') }}"
	nginx_http_redirect: false
	nginx_https: false
	nginx_sites_https: []
	nginx_call_certificate_role: "{{ nginx_https }}"
	nginx_http: true
	nginx_sites_http:
	  - name: 'GH_HIS'
	    internal_port: "None"
	    external_port: '80'
	    uwsgi: true
	    unix_socket_path: "{{ gh_uwsgi_instance.unix_socket_path | default('/opt/gnuhealth/var/run/gnuhealth.sckt') }}"
	  - name: 'Orthanc'
	    internal_port: '{{ or_port }}'
	    external_port: '81'
	    uwsgi: false
	    unix_socket_path: "None"
	  - name: 'Thalamus'
	    internal_port: "None"
	    external_port: '82'
	    uwsgi: true
	    unix_socket_path: "{{ uwsgi_th_instance.unix_socket_path | default('/opt/thalamus/var/run/thalamus.sckt') }}"

Finally we merge `psql_db_users`, `psql_dbs` and `psql_hba_rules` in postgresql/vars.yml:

::

	postgresql: true
	psql_db_users:
	  - name: "gnuhealth"
	    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
	  - name: "orthanc"
	    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
	  - name: "thalamus"
	    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
	psql_dbs:
	  - name: "health"
	    owner: "gnuhealth"
	    state: "present"
	    restore: false
	    path: "../fetch/dump.sql"
	  - name: "orthancdb"
	    owner: "orthanc"
	    state: "present"
	    restore: false
	    path: "../fetch/dump.sql"
	  - name: "federation"
	    owner: "thalamus"
	    state: "present"
	    restore: false
	    path: "../fetch/dump.sql"
	psql_listen_remote: "{{ psql_domain != nginx_application_domain | default(false) }}"
	psql_use_pw: "{{ psql_listen_remote }}"
	psql_use_cert: "{{ psql_listen_remote }}"
	psql_pw: "{{ vault_psql_pw | default('gnusolidario') }}"
	psql_db_template: "template0"
	psql_domain: "{{ groups['postgresql'][0] | default('localhost') }}"
	psql_hba_rules:
	- "local\tall\t{{ psql_db_users[0].name }}\t\t\t\t\tpeer"
	- "local\tall\t{{ psql_db_users[1].name }}\t\t\t\t\tpeer"
	- "local\tall\t{{ psql_db_users[2].name }}\t\t\t\t\tpeer"
	psql_shared_buffers: "{{ (ansible_memtotal_mb * 0.4) | int | abs }}MB"
	psql_encryption: 'scram-sha-256'
	psql_tls_min: 'TLSv1.2'
	psql_set_encoding: true
	psql_encoding: 'UTF-8'
	psql_set_locale: false
	psql_locale: 'en_US'
	psql_set_timezone: false
	psql_timezone: 'Europe/Berlin'
	psql_set_datestyle: false
	psql_datestyle: 'iso, mdy'
	psql_barman: "{{ backup | default(false) }}"
	psql_barman_sync: false
	psql_barman_super_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"
	psql_barman_replication_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"

In order to actually override the default configuration delete all the configuration blocks for uWSGI, Nginx & PostgreSQL inside inventories/dev/group_vars/{gnuhealth,orthanc,thalamus}/vars.yml (Orthanc does not have a uWSGI section anyway).

The last thing to change is inventories/dev/hosts. In order to load all variables in the group_vars folder we have to outcomment the line under `[gnuhealth]` and add `localhost` under `[gnuhealth_app]`, `[gnuhealth_psql]` and `[gnuhealth_nginx]`. Besides do the same for `orthanc` and `thalamus`. Finally change the line under `[desktop]` to `localhost` as well.

::

	[...]
	[gnuhealth]
	# hostname_gnuhealth ansible_port=22

	[...]

	[gnuhealth_app]
	localhost

	[gnuhealth_psql]
	localhost

	[gnuhealth_nginx]
	localhost

	[...]

Now we are ready to execute the playbook containing all the corresponding roles:

.. code-block:: console

  $ ansible-playbook playbooks/all_minimal.yml -i inventories/dev -c local -e ansible_user=`whoami` -K

The client can be tested just like in the first example.

Orthanc is accessible from the browser at `http://localhost:81` with the credentials `admin:admin`.

The welcome page of Thalamus can also get accessed using a browser at `http://localhost:82`.
