.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

mygnuhealth
===========

This role installs the mobile application MyGNUHealth on a GNU/Linux system using PyPI / pip.

For openSUSE Python 3.9 is installed in the first place because the default version 3.6 has EOL and MyGNUHealth expects 3.9 or higher.

MyGNUhealth is installed in a virtual environment and the is binary linked to /usr/local/bin/ to have it in the default path.

A desktop entry is created in the applications menu so you can also start it without using the terminal.

Note that this currently requires the PYTHONPATH environment variable set. If starting from terminal make sure to run `source ~/.bashrc` before calling `mygnuhealth`. This is only relevant for the session during which the Ansible playbook was called as the shell environment file gets called at the beginning of a session automatically.

- **Parameters**:

  - mygh_version: Version to install if using PyPI (on Debian)

- **Defaults**:

- **Variables**:

  - mygh_packages: Dict of system packages to install before pip installation, taking the OS family as key
  - mygh_venv: Path of virtual python environment
  - mygh_use_testpypi: If true use test.pypi.org, meant for testing before publishing to pypi.org
  - mygh_bash: Path of bash depending on OS (differs for FreeBSD)
  - mygh_icon: Path of the MyGNUHealth icon for a desktop entry, taken from PyPI package after installation by default
