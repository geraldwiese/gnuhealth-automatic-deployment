#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Run this from top level directory like this:
# bash tests/run_molecule.sh | tee test.log

count_test() {
    let COUNTER_TOTAL++
    echo $TEST_STRING
    if "$@"; then
        let COUNTER_SUCCESS++
    else
        let COUNTER_FAILED++
        echo "${TEST_STRING} failed" >> $FAIL_LOG
    fi
}

# Prepare
cd tests/playbook_allinone
COUNTER_TOTAL=0
COUNTER_FAILED=0
COUNTER_SUCCESS=0
FAIL_LOG="../../fail.log"

[ ! -f $FAIL_LOG ] || rm $FAIL_LOG
# Test example 1 (+ Orthanc & Thalamus similarly) & example 2
for playbook in "gnuhealth" "orthanc" "thalamus" "desktop"; do
    export PLAYBOOK_TO_TEST=${playbook}
    TEST_STRING="Testing playbook playbooks/${playbook}.yml for all in one system"
    count_test molecule test
done

# Test example 3 (+ Orthanc similarly)
cd ../playbook_split
for playbook in "gnuhealth" "orthanc"; do
    export PLAYBOOK_TO_TEST=${playbook}
    TEST_STRING="Testing playbook playbooks/${playbook}.yml for separated systems"
    count_test molecule test
done

# Test example 4
cd ../..
bash inventories/test/generate_one_system_multi_apps.sh
cd tests/one_system_multi_apps
TEST_STRING="Testing multiple applications on one system"
count_test molecule test

# Test example 5
cd ../..
bash inventories/test/generate_gnuhealth_prod_small.sh
# Overtake the majority of the example but no controller system exists thus importing its public SSH key would fail
cp inventories/dev/group_vars/ssh/vars.yml inventories/test/gnuhealth_prod_small/group_vars/ssh/vars.yml
export NEW_INVENTORY_PATH=inventories/test/gnuhealth_prod_small
export SSH_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/ssh/vars.yml
sed -i "s?^.*ssh_set_port:.*?ssh_set_port:\ true?" $SSH_VARS_PATH
sed -i "s?^.*ssh_port:.*?ssh_port:\ 63671?" $SSH_VARS_PATH
sed -i "s?^.*ssh_disable_pw_auth:.*?ssh_disable_pw_auth:\ true?" $SSH_VARS_PATH
sed -i "s?^.*ssh_set_log_level:.*?ssh_set_log_level:\ true?" $SSH_VARS_PATH
cd tests/gnuhealth_prod_small
TEST_STRING="Testing example for small productive base"
count_test molecule test

# Conclude
echo "total: ${COUNTER_TOTAL}"
echo "failed: ${COUNTER_FAILED}"
echo "success: ${COUNTER_SUCCESS}"
