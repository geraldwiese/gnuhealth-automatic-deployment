.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Contributing
============

Contributing to this project is more than welcome!

If you want to file issues, you can either create them on the GitLab project or on GNU Savannah:

https://gitlab.com/geraldwiese/gnuhealth-automatic-deployment

https://savannah.gnu.org/bugs/?group=health

For discussions you can join the chat or mailing lists of GNU Health:

https://docs.gnuhealth.org/his/support.html

Feel free to also open Merge Requests or ask for membership to open another developing branch.


Maintenance
-----------

Best is to run the BASH tests locally before any commit. You can find them in the `tests` folder.
In `.gitlab-ci.yml` you can find the way how they are called and their dependencies.

Molecule tests are more and more supersized for laptops - thus a dedicated test server is planned to run them periodically and before merges.

Before any merge into the main branch the following has to be updated and checked as well:

- VERSION in top level directory

- CHANGELOG in top level directory

- `version` variable in docs/source/conf.py

- Roadmap in the documentation

- Local built version of the documentation as in the pages stage inside .gitlab-ci.yml
