.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Backup & Restore
================

For your backup/restore strategy you should take into account the following:

- This project before and after your modifications

- If using a custom CA, its certificate and key, best the whole folder (/root/tls by default)

- PostgreSQL: Database

- Application data, for GNU Health: Attachments folder (/opt/gnuhealth/var/lib by default)

- Server certificates and corresponding private keys if obtained separately

- SSH keys

If your setup is distributed on multiple systems you can use the barman & restic roles of this repository for PostgreSQL & application data. Read the roles sections & documentations carefully and test the restore if you want to use them for productive systems. Keep in mind that Barman allows to achieve RPO of zero but Restic doesn't. You might even call its Cron job every minute but this would still imply up to a minute data loss. Thus you should decide upfront if you want to risk inconsitencies, risk a minute of data loss or find another way to achieve RPO of zero for your applications data folder as well.

The traditional `gnuhealth-control` command is also refreshed for backing up on mounted disks. But it can not handle distributed systems as it expects the database to be on the application system.

Follow these links for further informations for backup & restore:

https://en.wikibooks.org/wiki/GNU_Health/Control_Center

https://book.orthanc-server.com/users/backup.html

https://www.postgresql.org/docs/devel/backup-dump.html

https://www.postgresql.org/docs/14/continuous-archiving.html

https://docs.pgbarman.org/release/3.8.0/

https://restic.net/
