.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

unattended-upgrades
===================

This role handles Unattended Upgrades on Debian systems. 
It can be used to enable or disable automatic upgrades and potentially even reboots.

https://wiki.debian.org/UnattendedUpgrades

- **Parameters**:

  - uu_enable: If true, package is installed and service enabled. If false it is disabled.
  - uu_automatic_reboot: If true, reboot is performed after upgrades if necessary.
  - uu_automatic_reboot_time: If reboot is desired this sets the time when to perform a reboot.

- **Defaults**:

  - uu_template_comment: Comment put on top of every template delivered by Ansible
