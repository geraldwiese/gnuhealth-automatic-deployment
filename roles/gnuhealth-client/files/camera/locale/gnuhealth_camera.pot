# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnuhealth-plugin-gnuhealth_camera package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnuhealth-plugin-gnuhealth_camera 4.4.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-02-22 17:07+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Attachment is saved, Please refresh the view!"
msgstr ""

msgid ""
"========== GNU Health Camera Help ==========\n"
"\n"
"a = Attach media file in the current model\n"
"\n"
"[space] = Set the picture in model (when available)\n"
"\n"
"h = This help message\n"
"\n"
"q = Quit the camera application\n"
"\n"
msgstr ""

msgid "Please install \"opencv-python\" and restart gnuhealth-client."
msgstr ""

msgid "CV2 library is not found!"
msgstr ""

msgid "Please choose one record associated to this picture / video"
msgstr ""

msgid "You need to select a record !"
msgstr ""

msgid "Please choose only one record for this picture / video"
msgstr ""

msgid "Multiple records selectd !"
msgstr ""

msgid "Please ensure that your camera is working properly."
msgstr ""

msgid "No Camera is found!"
msgstr ""

msgid "GNU Health camera"
msgstr ""
