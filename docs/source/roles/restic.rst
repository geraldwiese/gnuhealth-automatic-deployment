.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

restic
======

This role installs restic in order to backup the data folders of application servers. It can handle backups with encryption, deduplication, snapshots, retention policies, etc.

https://restic.net/

On the server side only users are created to have a dedicated SSH access and directories.

On the client side at application servers first of all a password file is created and it is ensured that there is at least an empty hidden file in the target directories to avoid erros caused by empty directories. Then the repository gets initialized and Cron jobs are set up for backups and cleanup.

The restore does only work on empty directories as this is not intended to overwrite any data that might not be saved.

Note that Barman allows to achieve a Recovery Point Objective (RPO) of zero but Restic is called in a frequency defined as `rstc_client_backup_cron_minute` which is by default every 5 minutes. This can lead to inconsistencies.

- **Parameters**:

  - rstc_server_users: Users that are target of SSH connections to push backups
  - rstc_server_trust_ssh_key: SSH keys to trust for given users
  - rstc_client_user: OS username for using restic
  - rstc_client_target: Connection string used by the client to connect to the backup server
  - rstc_client_pw: Password used to protect and access the repository
  - rstc_client_pw_file: Path where to put the password file
  - rstc_client_backup_cron: If true run backups as periodic Cron jobs
  - rstc_client_backup_dir: Target data directory that is subject to backups
  - rstc_client_backup_cron_job: command executed by Cron job
  - rstc_client_backup_cron_minute: Parameter for cron modules minutes (default every 5 minutes)
  - rstc_client_cleanup_cron: If true run cleanup as Cron job
  - rstc_client_cleanup_cron_job: Cleanup command executed by Cron
  - rstc_client_cleanup_period: Period to run cleanup jobs (default daily)
  - rstc_client_restore: If true run restore command
  - rstc_client_restore_cmd: Actual restore bash command

- **Defaults**:

  - rstc_create_hidden_init_file: If true create hidden file to avoid empty directory error
  - rstc_hidden_init_file_name: Path of hidden file
