.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

gnuhealth-client
================

The client also gets installed using pip inside a virtual environment. 
Besides the connection informations for the server and databases are already entered in the config from the previous server deployment.
If `ghcl_add_community_server` is true an entry for the community server will be created as well.

This configuration and the plugin will be placed in /etc/skel/ in order to show up for newly created operating system users.
The crypto plugin also gets copied into the plugins directory.
If `ghcl_copy_from_skel` is true it will also be copied to the home directory of the current user set in `vault.yml`.

The executable is linked to /usr/local/bin/gnuhealth-client in order to be executable without activating the virtual environment first.
An desktop entry is also created as alternative to run the client from the terminal.
By default the password for the admin user is `gnusolidario`. It is set in `group_vars/gnuhealth/vault.yml` in the inventory.

Besides the documentation is downloaded and wrapped into a desktop entry as well.

The client does not have an option to ignore security risks for unknown certificates.
If you created a custom CA, set `ghcl_trust_certs` to true in order to trust the certificate.

- **Parameters**:

  - ghcl_version: Version to install of the PyPI package gnuhealth-client
  - ghcl_crypto_plugin: If true install the crypto plugin which implies to install `python-gnupg` inside `ghcl_venv` and copy the plugin itself into `ghcl_plugins_path`.
  - ghcl_camera_plugin: If true install the camera plugin which implies to install `opencv-python` inside `ghcl_venv` and copy the plugin itself into `ghcl_plugins_path`.
  - ghcl_profile_name: Name of the profile for your new database
  - ghcl_host_domain: Domain name followed by `:port` of the HIS server
  - ghcl_db: Name of the new database
  - ghcl_tryton_user: Tryton user to connect to the database
  - ghcl_demo_db: If the local demo database should show up
  - ghcl_profile_demodb: Profile name for local demo database
  - ghcl_demo_db_name: Database name of local demo database
  - ghcl_add_community_server: If community server should show up for remote demo database
  - ghcl_add_offlince_docs: If documentation should be downloaded to have it accessible independent from your internet connection. Will have a desktop entry too.
  - ghcl_trust_certs: If a certificate of the server should be added to the systems trusted CAs
  - ghcl_certs: Path to certificate(s) to add to trusted CAs
  - ghcl_include_his_vars: Whether variables should be included from the server installation
  - ghcl_his_vars_file: Path to variables from server installation
  - ghcl_create_local_profile: Whether the profile entry containing the variables on top should be created for the client

- **Defaults**:

  - ghcl_template_comment: Comment put on top of every template delivered by Ansible

- **Variables**:

  - ghcl_packages: Packages to be installed on the system differentiated by OS family
  - ghcl_camera_packages: Packages to install for using the camera plugin
  - ghcl_plugins_path: Global path for plugins, links are distributed to users directory with this as target
  - ghcl_copy_from_skel: If config, plugins and offlince docs should directly get copied from /etc/skel to `ansible_user` home directory
  - ghcl_crypto_dir_name: Name of the crypto plugin directory inside `ghcl_plugins_path`
  - ghcl_camera_dir_name: Name of the camera plugin directory inside `ghcl_plugins_path`
  - ghcl_venv: Path of the Python virtual environment used for installing the client and plugins dependencies
  - ghcl_use_testpypi: Whether TestPyPI should be used instead of PyPI, this can be used for testing the `gnuhealth-client` package before publishing
  - ghcl_community_db: Name of the demo database on the community server
  - ghcl_icon_path: Path of an icon that is put during deployment and used by desktop entries
  - ghcl_docs_url: URL to download documentation from
  - ghcl_docs_target: Target for documentation download
  - ghcl_docs_exe: Path of global index.html to be opened by offline docs desktop entry
