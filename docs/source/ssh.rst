.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Securing SSH & passwords
========================
If your server accepts SSH connections, the playbooks work directly if you specify the right domain/IP, username and
password. However you could consider securing SSH:

Enable host key checking
------------------------
* Change `vault_ssh_args` to '-o StrictHostKeyChecking=yes'

* Add the hosts fingerprint to known_hosts in ssh:

.. code-block:: console

    $ ssh-keyscan -H domain >> ~/.ssh/known_hosts

* Login to the server and make sure the fingerprints match


Disable password-based ssh login and root login
-----------------------------------------------
* Generate a ssh key if necessary:

.. code-block:: console

    $ ssh-keygen -t rsa

* Copy the public key to the server:

.. code-block:: console

    $ ssh-copy-id -i ~/.ssh/key_rsa.pub user@server

(check in ~/.ssh/authorized_keys at server)

* To disable password-based login and root login edit /etc/ssh/sshd_config as sudo:

::

    ChallengeResponseAuthentication no
    PasswordAuthentication no
    UsePAM no
    PermitRootLogin no

* You might need to setup ssh-agent for ansible because it does not prompt for a password to unlock the private key (or try to make it prompt for the password):

.. code-block:: console

    $ ssh-agent bash
    $ ssh-add ~/.ssh/id_rsa
        (ssh private key path)
    $ exit

* Try connecting:

.. code-block:: console

    $ ansible gnuhealth -m ping -e "ansible_user='user'"

Handling the sudo password
--------------------------
If you use a password stored in vault you could at least encrypt the vault file like it is
explained in the section `Encryption by Ansible-Vault`.

In order to avoid putting your sudo password in vault two different approaches for becoming sudo are tested:

* Keep `vault_set_sudo_pass` false and run the playbook with the `-K` flag (default)

* Enable passwordless sudo on the target system and set `vault_set_sudo_pass` to false as well.
  For example `sudo visudo /etc/sudoers/` and change the line `%sudo   ALL=(ALL:ALL) ALL`
  to `%sudo  ALL=(ALL:ALL) NOPASSWD: ALL` on Debian. For openSUSE use `wheel` instead of `sudo` and add your user to
  wheel: `sudo usermod -aG wheel username`.

Prompt for SSH password
-----------------------

If you do not have a SSH key but want to avoid putting the password in `vault.yml` use the `-k` flag when calling the playbook.
Additionally you have to remove the line `- ansible_ssh_pass: "{{ auth_ssh_password }}"` from the playbook gnuhealth.yml or another one.

Using the SSH playbook
----------------------

If you want to create and fetch a key on the Ansible controller in order to trust it on target servers, run the playbook with the following flags:

.. code-block:: console

    $ ansible-playbook playbooks/ssh.yml -i inventories/dev -c local -e ssh_key_create=1 -e ssh_key_user=`whoami` -e ssh_key_fetch=1 -K

Afterwards you should have the key in the fetch folder.

Now set the host(s) where the new key should get trusted in inventories/dev/hosts under [ssh]. 
Next open inventories/dev/group_vars/ssh/vars.yml and uncomment the lines under `ssh_key_trust_local_paths`, remove the `[]` above, set `key` to the path of the key in the fetch folder and `user` to your username.
Now run the playbook again:

.. code-block:: console

    $ ansible-playbook playbooks/ssh.yml -i inventories/dev -K -k

Afterwards you should be able to connect using the SSH key.

Note that you need the `sshpass` program installed for connecting over SSH password based. It can be installed from terminal:

.. code-block:: console

    $ sudo apt install sshpass

If you want to disable password based authentication set `ssh_disable_pw_auth` to true and run the playbook again.
