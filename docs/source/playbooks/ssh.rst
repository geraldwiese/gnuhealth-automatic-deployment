.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

ssh
===

This playbook only calls the SSH role. If you can already reach your servers using SSH password based you can use it e.g. to trust keys and disable password authentication.

Check the SSH role and the chapter "Securing SSH & passwords" for more information.
