# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

- name: Check if demo database exists already
  ansible.builtin.command: psql -l
  register: psql_dbs
  changed_when: false
  become_user: postgres
  become: true
  become_method: "{{ psql_become_method_unprivileged_users }}"
  become_exe: "{{ psql_become_exe_unprivileged_users }}"
- name: Get demo database
  become_user: postgres
  become: true
  become_method: "{{ psql_become_method_unprivileged_users }}"
  become_exe: "{{ psql_become_exe_unprivileged_users }}"
  when: psql_gh_demo_db_name not in psql_dbs.stdout
  block:
    - name: Download demo database
      ansible.builtin.get_url:
        url: "https://www.gnuhealth.org/downloads/postgres_dumps/gnuhealth-{{ psql_gh_demo_db_version }}-demo.sql.gz"
        dest: /tmp/
        mode: '0664'
    - name: Check if uncompressed already
      ansible.builtin.stat:
        path: /tmp/gnuhealth-{{ psql_gh_demo_db_version }}-demo.sql
      register: psql_demodb
    - name: Extract demo database
      ansible.builtin.command:
        chdir: /tmp/
        cmd: gunzip -q gnuhealth-{{ psql_gh_demo_db_version }}-demo.sql.gz
      when: not psql_demodb.stat.exists
      changed_when: true
    - name: Create demo database
      community.postgresql.postgresql_db:
        name: '{{ psql_gh_demo_db_name }}'
        owner: "{{ psql_gh_db_user }}"
        template: "template0"
        encoding: "{{ psql_encoding if psql_set_encoding else '' }}"
        lc_collate: "{{ (psql_locale + '.' + psql_encoding) if psql_set_locale else '' }}"
        lc_ctype: "{{ (psql_locale + '.' + psql_encoding) if psql_set_locale else '' }}"
    - name: Fill demo database
      community.postgresql.postgresql_db:
        name: '{{ psql_gh_demo_db_name }}'
        owner: "{{ psql_gh_db_user }}"
        state: restore
        target: "/tmp/gnuhealth-{{ psql_gh_demo_db_version }}-demo.sql"
