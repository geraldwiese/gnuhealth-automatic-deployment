# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from proteus import config
print('Connecting to the server using HTTPS...')
pw = os.environ['TRYTONPW']
conf_string = 'https://admin:' + pw + '@server/health/'
conf = config.set_xmlrpc(conf_string)
print('Success')
