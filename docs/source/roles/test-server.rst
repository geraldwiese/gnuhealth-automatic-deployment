.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

test-server
===========

This role is setting up a test server for running the molecule integration tests.

They are based on QEMU/KVM/Libvirt Virtual Machines.

Default values for the internal management network make sure that they also work for nested cases.

A user is created to have Libvirt access without having root access for running the tests.

From the git repository the main and latest develop branch is cloned.

Both have a small and a large test suite.

A shell script wraps the tests delivered by the repo in order to write emails using the result and execution time.

Finally Cron jobs are created out of those tests.

- **Parameters**:

  - ts_git_folder: Global folder to put cloned repositories and shell scripts inside
  - ts_clone: Dictionary for cloning main and develop branch. Subdictionaries require URL, branch and file path for the target system.
  - ts_tests: List of dictionaries to define different tests. Runs ``bash_script`` inside ``path``. Expects to get ``fail_log`` and ``test_log`` created.
    Uses ``name`` and ``period`` for the cron job.
  - ts_target_mail: Email target for results of tests.


- **Variables**:

  - ts_system_packages: Packages to be installed using apt.
  - ts_pypi_packages: Packages to be installed using pip.
  - ts_venv: Virtual environment for pip installation, created using "python3 -m venv" approach.
  - ts_user: Newly created user for running tests.
  - ts_groups: Groups the user needs to be part of for creating VMs.
  - ts_libvirt_network_name: Name of Libvirt network managed by Vagrant. Passed to molecule test to allow nested scenarios (don't use default values already used by host of host).
  - ts_libvirt_network_subnet: IP address range of Libvirt network managed by Vagrant. Reason see above.
