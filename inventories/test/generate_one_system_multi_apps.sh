#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

echo "Current working directory and its content:"
pwd
ls

echo "Setting paths"
export NEW_INVENTORY_PATH=inventories/test/one_system_multi_apps
export GH_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/gnuhealth/vars.yml
export TH_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/thalamus/vars.yml
export OR_VARS_PATH=$NEW_INVENTORY_PATH/group_vars/orthanc/vars.yml

echo "Recreating test inventory from dev"
[ ! -d $NEW_INVENTORY_PATH ] || rm -r $NEW_INVENTORY_PATH
cp -r inventories/dev $NEW_INVENTORY_PATH

echo "Creating and filling group_vars directories application, nginx & postgresql"
mkdir $NEW_INVENTORY_PATH/group_vars/{application,nginx,postgresql}
cat > $NEW_INVENTORY_PATH/group_vars/application/vars.yml <<EOL
uwsgi_instances:
  - "{{ th_uwsgi_instance }}"
  - "{{ gh_uwsgi_instance }}"
gh_psql_db_user: 'gnuhealth'
gh_psql_db_name: 'health'
th_psql_db_user: 'thalamus'
th_psql_db_name: 'federation'
or_psql_db_user: 'orthanc'
or_psql_db_name: 'orthancdb'
EOL
cat > $NEW_INVENTORY_PATH/group_vars/nginx/vars.yml <<EOL
nginx: true
nginx_rproxy_domain: "{{ groups['nginx'][0] | default('localhost') }}"  # for certificate & reverse proxy
nginx_application_domain: "{{ groups['application'][0] | default('localhost') }}"
nginx_http_redirect: false
nginx_https: false
nginx_sites_https: []
nginx_call_certificate_role: "{{ nginx_https }}"
nginx_http: true
nginx_sites_http:
  - name: 'GH_HIS'
    internal_port: "None"
    external_port: '80'
    uwsgi: true
    unix_socket_path: "{{ gh_uwsgi_instance.unix_socket_path | default('/opt/gnuhealth/var/run/gnuhealth.sckt') }}"
  - name: 'Orthanc'
    internal_port: '{{ or_port }}'
    external_port: '81'
    uwsgi: false
    unix_socket_path: "None"
  - name: 'Thalamus'
    internal_port: "None"
    external_port: '82'
    uwsgi: true
    unix_socket_path: "{{ uwsgi_th_instance.unix_socket_path | default('/opt/thalamus/var/run/thalamus.sckt') }}"
EOL
cat > $NEW_INVENTORY_PATH/group_vars/postgresql/vars.yml <<EOL
postgresql: true
psql_db_users:
  - name: "gnuhealth"
    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
  - name: "orthanc"
    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
  - name: "thalamus"
    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
psql_dbs:
  - name: "health"
    owner: "gnuhealth"
    state: "present"
    restore: false
    path: "../fetch/dump.sql"
  - name: "orthancdb"
    owner: "orthanc"
    state: "present"
    restore: false
    path: "../fetch/dump.sql"
  - name: "federation"
    owner: "thalamus"
    state: "present"
    restore: false
    path: "../fetch/dump.sql"
psql_listen_remote: "{{ psql_domain != nginx_application_domain | default(false) }}"
psql_use_pw: "{{ psql_listen_remote }}"
psql_use_cert: "{{ psql_listen_remote }}"
psql_pw: "{{ vault_psql_pw | default('gnusolidario') }}"
psql_db_template: "template0"
psql_domain: "{{ groups['postgresql'][0] | default('localhost') }}"
psql_hba_rules:
  - "local\tall\t{{ psql_db_users[0].name }}\t\t\t\t\tpeer"
  - "local\tall\t{{ psql_db_users[1].name }}\t\t\t\t\tpeer"
  - "local\tall\t{{ psql_db_users[2].name }}\t\t\t\t\tpeer"
psql_shared_buffers: "{{ (ansible_memtotal_mb * 0.4) | int | abs }}MB"
psql_encryption: 'scram-sha-256'
psql_tls_min: 'TLSv1.2'
psql_set_encoding: true
psql_encoding: 'UTF-8'
psql_set_locale: false
psql_locale: 'en_US'
psql_set_timezone: false
psql_timezone: 'Europe/Berlin'
psql_set_datestyle: false
psql_datestyle: 'iso, mdy'
psql_barman: "{{ backup | default(false) }}"
psql_barman_sync: false
psql_barman_super_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"
psql_barman_replication_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"
EOL

echo "Remove values from services vars.yml to be overriden by previously created files"
sed -i '/^##\ uWSGI/,/^#\ Certificates/{/^#/!d;}' $GH_VARS_PATH
sed -i '/^##\ uWSGI/,/^#\ Certificates/{/^#/!d;}' $TH_VARS_PATH
sed -i '/^##\ PostgreSQL/,/^#\ Certificates/{/^#/!d;}' $OR_VARS_PATH

echo "Create hosts file"
cat > $NEW_INVENTORY_PATH/hosts <<EOL
[gnuhealth]

[gnuhealth:children]
gnuhealth_app
gnuhealth_psql
gnuhealth_nginx

[gnuhealth_app]
debian12

[gnuhealth_psql]
debian12

[gnuhealth_nginx]
debian12


[orthanc]

[orthanc:children]
orthanc_app
orthanc_psql
orthanc_nginx

[orthanc_app]
debian12

[orthanc_psql]
debian12

[orthanc_nginx]
debian12


[thalamus]

[thalamus:children]
thalamus_app
thalamus_psql
thalamus_nginx

[thalamus_app]
debian12

[thalamus_psql]
debian12

[thalamus_nginx]
debian12


[application:children]
gnuhealth_app
orthanc_app
thalamus_app

[postgresql:children]
gnuhealth_psql
orthanc_psql
thalamus_psql

[nginx:children]
gnuhealth_nginx
orthanc_nginx
thalamus_nginx

EOL
