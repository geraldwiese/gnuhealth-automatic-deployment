.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Tests
=====

Numerous tests are defined in the `tests` folder and called from `.gitlab-ci.yml`.
Some tests are simple BASH scripts and used for:

- Checking if the original configuration files used for templates are up to date

- Linting Ansible code

- Checking if the project fulfills the REUSE compliance (if every single file contains license & copyright information)

- Ensuring consistency for variables used the same way across the project

Other tests are build on Molecule, VirtualBox and Vagrant.
GNU Health, Orthanc & Thalamus are tested for all-in-one-systems for Ubuntu, Debian, openSUSE Leap and FreeBSD for the minimal default installation.
GNU Health is also tested for separated systems.
In any case Molecule performs syntax and idempotency checks.
Integration tests were added as well: They test if the connection works when sending a request to the reverse proxy that has to be processed by the application and the database as well (`verify` in Molecule).

Currently the molecule tests are not triggered by commits because they require a shell executor and this implies security issues.
But they are executed periodically.

| REUSE by the Free Software Foundation Europe:
| https://reuse.readthedocs.io/en/latest/

| Molecule for testing Ansible:
| https://molecule.readthedocs.io/en/latest/
