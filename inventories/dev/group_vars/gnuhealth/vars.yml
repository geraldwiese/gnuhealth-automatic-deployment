# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

---
# Authentication
auth_os_user: "{{ vault_ssh_user }}"
auth_ssh_args: "{{ vault_ssh_args }}"
auth_ssh_password: "{{ vault_ssh_password }}"
auth_sudo_passwords: "{{ vault_sudo_pws }}"
auth_set_sudo_pass: "{{ vault_set_sudo_pass }}"


# Application
application: true
application_https: "{{ nginx_rproxy_domain != nginx_application_domain }}"

## GNU Health
gh_version: 4.4.0.3
gh_psql_uri: "{{ gh_psql_uri_local if (psql_domain | default('localhost')) == (nginx_application_domain | default('localhost')) else gh_psql_uri_remote }}"
gh_demo_db: false
gh_webdav: false
gh_cron: false
gh_tryton_admin_mail: 'example@example.com'
gh_modules:
  - health
  # - health_surgery
gh_update_weekly: false
gh_restart_service_after_update: false
# Want to connect to previously deployed Orthanc server with ownca cert?
gh_trust_cert_orthanc: false
gh_trust_certs:
  - ../fetch/orthanc-custom-ca.crt
gh_tryton_pw: "{{ vault_tryton_pw | default('gnusolidario') }}"
gh_ssh_key: "{{ backup | default(false) }}"

## uWSGI
uwsgi_instances:
  - "{{ gh_uwsgi_instance }}"
uwsgi_upgrade: false

# PostgreSQL
postgresql: true
psql_db_users:
  - name: "gnuhealth"
    flags: "CREATEDB,NOCREATEROLE,NOSUPERUSER"
psql_dbs:
  - name: "health"
    owner: "gnuhealth"
    state: "present"
    restore: false
    path: "../fetch/dump.sql"
psql_listen_remote: "{{ ((psql_domain | default('localhost')) != (nginx_application_domain | default('localhost'))) or backup | default(false) }}"
psql_use_pw: "{{ psql_listen_remote }}"
psql_use_cert: "{{ psql_listen_remote }}"
psql_pw: "{{ vault_psql_pw | default('gnusolidario') }}"
psql_db_template: "template0"
psql_domain: "{{ groups['postgresql'][0] | default('localhost') }}"
psql_hba_rules:
  - "local\tall\t{{ psql_db_users[0].name }}\t\t\t\t\tpeer"  # DB system == appl system, otherwise take line below
  # - "hostssl\t{{ psql_dbs[0].name }},{{ gh_demo_db_name }},{{ psql_db_template }}\t{{ psql_db_users[0].name  }}\t192.168.0.111/32\tscram-sha-256"
  # - "host\treplication\t{{ psql_barman_replication_user }}\t192.168.0.100/32\tscram-sha-256"  # Barman backup streaming
  # - "host\tall\t\t{{ psql_barman_super_user }}\t\t\t192.168.0.100/32\tscram-sha-256"  # Barman backup streaming
psql_shared_buffers: "{{ (ansible_memtotal_mb * 0.4) | int | abs }}MB"
psql_encryption: 'scram-sha-256'
psql_tls_min: 'TLSv1.2'
psql_set_encoding: false
psql_encoding: 'UTF-8'
psql_set_locale: false
psql_locale: 'en_US'
psql_set_timezone: false
psql_timezone: 'Europe/Berlin'
psql_set_datestyle: false
psql_datestyle: 'iso, mdy'
psql_barman: "{{ backup | default(false) }}"
psql_barman_host: "{{ groups['bak'][0] | default('barman_hostname') }}"
psql_barman_host_port: 22
psql_barman_sync: false
psql_barman_super_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"
psql_barman_replication_pw: "{{ vault_psql_pw_barman | default('gnusolidario') }}"


# Nginx as reverse proxy
nginx: true
nginx_rproxy_domain: "{{ groups['nginx'][0] | default('localhost') }}"  # for certificate & reverse proxy
nginx_application_domain: "{{ groups['application'][0] | default('localhost') }}"
nginx_http_redirect: false
nginx_https: false
nginx_sites_https:
  - name: 'GH_HIS'
    internal_port: "{{ gh_uwsgi_instance.port | default('8443') }}"
    external_port: '443'
    uwsgi: true
    unix_socket_path: "{{ gh_uwsgi_instance.unix_socket_path | default('/opt/gnuhealth/var/run/gnuhealth.sckt') }}"
  # - {name: 'GH_WebDAV', internal_port: '{{ gh_webdav_port }}', external_port: '8443', uwsgi: false, unix_socket_path: 'None'}
nginx_call_certificate_role: "{{ nginx_https }}"
nginx_http: true
nginx_sites_http:
  - name: 'GH_HIS'
    internal_port: "{{ gh_uwsgi_instance.port | default('8443') }}"
    external_port: '80'
    uwsgi: true
    unix_socket_path: "{{ gh_uwsgi_instance.unix_socket_path | default('/opt/gnuhealth/var/run/gnuhealth.sckt') }}"


# Certificates
certificate_authority: "{{ nginx_https or application_https or psql_use_cert }}"
cert_ownca:
  create_ca: true
  type: 'ownca'
  # cert_params for [country, state, town, organization, subsection, common name, filename without extension (.crt, .key)]
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit ca', "{{ inventory_hostname }}", "{{ groups['ownca'][0] if groups['ownca'] is defined and groups['ownca'] | length > 0 else inventory_hostname }}-CA"]
  cert_admin_mail: 'example@example.com'
  revoke: []
    # - "{{ cert_ssl_paths.cert_path }}{{ cert_nginx.cert_params[-1] }}.crt"
cert_nginx:
  type: 'ownca'  # available types: 'ownca', 'copy' & 'letsencrypt'
  # only for 'ownca'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit web', '{{ nginx_rproxy_domain | default(inventory_hostname) }}', '{{ inventory_hostname }}-nginx']
  renew: false
  # only for letsencrypt
  revoke: false
  # for 'ownca' and 'letsencrypt'
  cert_admin_mail: 'example@example.com'
  # only for 'copy'
  # set remote_source true if cert + key are already on the remote system
  remote_source: false
  my_cert: "path/filename.crt"
  my_key: "path/filename.key"
cert_application:
  type: 'ownca'  # available types: 'ownca' or 'copy'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit app', '{{ nginx_application_domain | default(inventory_hostname) }}', '{{ inventory_hostname }}-app']
  renew: false
  cert_admin_mail: 'example@example.com'
  remote_source: false
  my_cert: "path/filename.crt"
  my_key: "path/filename.key"
cert_psql:
  type: 'ownca'  # available types: 'ownca' or 'copy'
  cert_params: ['DE', 'state', 'locality', 'organization', 'organizational unit db', '{{ psql_domain | default(inventory_hostname) }}', '{{ inventory_hostname }}-psql']
  renew: false
  cert_admin_mail: 'example@example.com'
  remote_source: false
  my_cert: "path/filename.crt"
  my_key: "path/filename.key"


# Backup system
backup: false

## Barman for PostgreSQL
brm_psql_host: "{{ groups['postgresql'][0] | default('db') }}"
brm_psql_super_user: "{{ psql_barman_super_user | default('barman') }}"
brm_psql_replication_user: "{{ psql_barman_replication_user | default('streaming_barman') }}"
brm_psql_super_pw: "{{ psql_barman_super_pw | default('gnusolidario') }}"
brm_psql_replication_pw: "{{ psql_barman_replication_pw | default('gnusolidario') }}"
brm_psql_config_name: "streaming"
brm_retention_window: "1 MONTHS"
brm_min_backups: 3
brm_max_backup_age: "1 MONTHS"
brm_cron_base_backup: true
brm_cron_base_backup_period: "monthly"
brm_cron_job: "barman backup {{ brm_psql_config_name }}"
brm_trust_ssh_key:
  key: "../fetch/postgres-gh.pub"
  user: "{{ brm_psql_super_user }}"
brm_ssh_port: 22
brm_ssh_port_psql: 22
brm_restore: false
brm_restore_handle_service: true
brm_restore_psql_dir: "/var/lib/postgresql/15/main"
brm_restore_cmd: "barman recover --get-wal --remote-ssh-command 'ssh -p {{ brm_ssh_port_psql }} postgres@{{ brm_psql_host }}' {{ brm_psql_config_name }} latest {{ brm_restore_psql_dir }}"

## Restic for application
rstc_server_users:
  - name: "{{ gh_ssh_key_remote_user | default('gnuhealthbak') }}"
    home: "/opt/{{ gh_ssh_key_remote_user | default('gnuhealthbak') }}"
    password: "{{ vault_gh_bak_password | default('gnusolidario') }}"
    salt: "{{ vault_gh_bak_salt | default('gnusaltidario') }}"
    shell: "/bin/bash"
rstc_server_trust_ssh_key:
  key: "../fetch/gnuhealth.pub"
  user: "{{ rstc_server_users[0].name }}"
rstc_client_user: "gnuhealth"
rstc_client_target: "sftp://{{ rstc_server_users[0].name }}@{{ groups['bak'][0] }}:22/restic-repo"
rstc_client_pw: "{{ vault_rstc_pw | default('gnusolidario') }}"
rstc_client_pw_file: "{{ gh_home_path | default('/opt/gnuhealth') }}/.restic_password"
rstc_client_backup_cron: true
rstc_client_backup_dir: "{{ gh_data_path | default('/opt/gnuhealth/var/lib') }}"
rstc_client_backup_cron_job: "cd {{ rstc_client_backup_dir }} && restic -r {{ rstc_client_target }} --password-file {{ rstc_client_pw_file }} backup ."
rstc_client_backup_cron_minute: "*/5"
rstc_client_cleanup_cron: true
rstc_client_cleanup_cron_job: "restic -r {{ rstc_client_target }} --password-file {{ rstc_client_pw_file }} forget --keep-within-hourly 1d --keep-within-daily 7d --keep-within-weekly 3w --keep-within-monthly 3m --keep-within-yearly 2y"
rstc_client_cleanup_period: "daily"
rstc_client_restore: false
rstc_client_restore_cmd: "restic -r {{ rstc_client_target }} --password-file {{ rstc_client_pw_file }} restore latest --target {{ rstc_client_backup_dir }}"


# Monitoring system Zabbix
zabbix: false
zbx_server_hostname: "{{ ansible_hostname }}"
zbx_server_ip: "192.168.60.101,127.0.0.1"
zbx_server_port: 8080
zbx_os: "ubuntu"
zbx_os_version: "22.04"
zbx_major_version: "6.0"
zbx_minor_version: "6.0-4"
zbx_repo: "https://repo.zabbix.com/zabbix/{{ zbx_major_version }}/{{ zbx_os }}/pool/main/z/zabbix-release/zabbix-release_{{ zbx_minor_version }}+{{ zbx_os }}{{ zbx_os_version }}_all.deb"  # noqa yaml[line-length]
zbx_db_user: "zabbix"
zbx_psql_use_pw: true
zbx_psql_pw: "{{ vault_zbx_psql_pw | default('gnusolidario') }}"
zbx_db_name: "zabbix"


# Miscellaneous

## Locale
locale: false
lcl_install:
  - 'de_DE.UTF-8'
lcl_set:
  - name: 'LANG'
    value: 'de_DE.UTF-8'
  - name: 'LC_MESSAGES'
    value: 'POSIX'
lcl_path: '/etc/default/locale'

## Fail2Ban
fail2ban: false
f2b_banaction: "nftables-allports"
f2b_use_mail: false
f2b_admin_mail: "example@example.com"
f2b_nginx: false
f2b_nginx_ports: "http,https"

## Unattended Upgrades
unattended_upgrades: false
uu_enable: true
uu_automatic_reboot: false
uu_automatic_reboot_time: "02:00"

## Postfix mail gateway
postfix: false
pf_mailname: '{{ ansible_hostname }}'
pf_mailtype: 'Satellite system'
pf_relayhost: 'example.com'
pf_mailto: 'example@example.com'

## sSMTP
ssmtp: false
ssmtp_mail_hub: "smtp.web.de:465"
ssmtp_mail_address: "example@example.com"
ssmtp_mail_pw: "{{ vault_mail_pw }}"

## Logwatch
logwatch: false
lw_detail: "High"
lw_debug: "Low"
lw_custom_filters:
  - name: nginx
    title: "Nginx web server"
    logfiles:
      - "nginx/*access.log"
      - "nginx/*access.log.1"
      - "nginx/*error.log"
      - "nginx/*error.log.1"
    archives:
      - "nginx/*access.log.*.gz"
      - "nginx/*error.log.*.gz"
  - name: gnuhealth
    title: "GNU Health application server"
    logfiles:
      - "/opt/gnuhealth/var/log/gnuhealth.log"
      - "/opt/gnuhealth/var/log/uwsgi.log"
    archives: []
lw_ssmtp: true
lw_mailto: "example@example.com"
lw_mailfrom: "{{ ssmtp_mail_address | default('example@example.com') }}"

## Systemd email alert
systemd_alert: false
sa_admin_mail: "example@example.com"
sa_monitor_services:
  - gnuhealth
  - nginx
  - postgresql
sa_ignore_mail_fail_in_service: true

## Set timezone
timezone: false
tz_timezone: 'Europe/Berlin'

## nftables
nftables: false
nft_network_interface: "{eth0, eth1}"
nft_internal_subnet: "192.168.60.0/24"
nft_allow_input_lines:
  - "iif {{ nft_network_interface }} tcp dport {80, 443} ip saddr {{ nft_internal_subnet }} accept"
  - "iif {{ nft_network_interface }} tcp dport 22 accept"
...
