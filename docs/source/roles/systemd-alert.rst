.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

systemd-alert
=============

With this role you can set systemd services that should be monitored.
For everyone of those services a drop-in config file will be added in order to trigger emails when the service is stopped and started.
This requires a working `sendmail` command.

- **Parameters**:

  - sa_admin_mail: Email address that should receive email alerts
  - sa_monitor_services: Systemd services to get drop-in config files for alerts
  - sa_ignore_mail_fail_in_service: If true mail errors are ignored in order not to break a service by its monitoring

- **Defaults**:

  - sa_template_comment: Comment put on top of every template delivered by Ansible
  - sa_timeout: Number of seconds for the timeout if `sa_ignore_mail_fail_in_service` is true
