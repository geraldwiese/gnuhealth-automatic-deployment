.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

timezone
========

With this role you can set a timezone in order to have the same in all your machines. This will trigger a restart of systemd-timesyncd and cron.

- **Parameters**:

  - tz_timezone: Timezone you want to set system wide on the server

- **Defaults**:

- **Variables**:

  - tz_packages: Packages to install system wide
