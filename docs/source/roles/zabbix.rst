.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

zabbix
======

This role contains installation and configuration steps for the monitoring system Zabbix. It can be used for both the server and the agents.
It is used in combination with the PostgreSQL and Nginx roles as you can see in the playbooks.
Use the boolean `zabbix` to control if this is actually used.

| For further information about Zabbix check its documentation:
| https://www.zabbix.com/documentation/6.0/en

The role comes with the following configuration options:

- **Parameters**:

  - zbx_server_hostname: The name that is passed to the Nginx role
  - zbx_server_ip: IP addresses accepted as server (for agents config)
  - zbx_server_port: Port to open for server
  - zbx_os: Operating system in use (only for download link)
  - zbx_os_version: Operating system version (only for download link)
  - zbx_major_version: Zabbix version major (only for download link)
  - zbx_minor_version: Zabbix version including minor (only for download link)
  - zbx_repo: Link for downloading Zabbix repo
  - zbx_db_user: PostgreSQL user name
  - zbx_psql_use_pw: If PostgreSQL user should have a password set
  - zbx_psql_pw: PostgreSQL password for the new user
  - zbx_db_name: Name of PostgreSQL database

- **Variables**:

  - zbx_packages: Packages to install for the server
  - zbx_db_import_from: Where to import database scheme from
  - zbx_config_main: Path of main config file
  - zbx_config_nginx: Path of Nginx config file
  - zbx_config_agent: Path of Agent config file
  - zbx_nginx_link: Path of Nginx link to remove
  - zbx_services: Services to start and enable for the server
  - zbx_agent_packages: Packages to install for the agent
  - zbx_agent_services: Services to start and enable for the agent
