.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

test-server
===========

This playbook bascially just executes the role test-server to set up the testing server.

Additionally either the postfix or the ssmtp role is used to be able to send emails.

The inventory ``inventories/test/test_server`` should be used with it and potential SSH or sudo passwords should only be used with prompts - if necessary at all.

You can call it like this (if those passwords are needed):

.. code-block:: console

    $ ansible-playbook playbooks/test_server.yml -i inventories/test/test_server/ -K -k
