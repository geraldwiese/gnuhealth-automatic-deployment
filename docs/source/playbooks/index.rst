.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Playbooks
=========

The following chapters describe the playbooks of this project.

There is a common structure for all playbooks which is explained using the example of `playbooks/gnuhealth.yml` and `inventories/dev`:

On the top of the playbook you can see a line `hosts: gnuhealth`. This group of hosts is defined in `inventories/dev/hosts`.
Besides Ansible loads variable files for all matching groups of a host from `inventories/dev/group_vars`.
As a simple alternative to `gnuhealth.yml` you can also call `gnuhealth_minimal.yml` which only contains the installtion with default configuration options without the roles for further system administration.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gnuhealth
   desktop
   orthanc
   thalamus
   all
   ssh
   test-server
