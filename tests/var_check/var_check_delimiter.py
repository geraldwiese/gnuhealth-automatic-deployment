# SPDX-FileCopyrightText: 2023 Jonathan Feilmeier
#
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml
from yaml.loader import SafeLoader

from pathlib import Path

from sys import stderr
from sys import argv, exit
from sys import exit

import argparse
import re
import io

# Functions
def path_exists(path):
    path = Path(path)

    if not path.exists():
        print('\033[31m' + str(path) + ' does not exist', file=stderr)
        raise SystemExit(1)

def check_missing_keys(data, keys, file_name):
    """
    Checks if the provided keys are present in the data. Returns and prints the missing keys.
    """
    missing_keys = [key for key in keys if key not in data]
    if missing_keys:
        print(f'\033[31mMissing keys in {file_name}: {", ".join(missing_keys)}\033[0m')
    return missing_keys

def load_yaml_delim(f, start_delimiter, end_delimiter, verbose=False):
    """
    Parse yml file with delimiters. Only Vars between start_delimiter and end_delimiter are parsed_contenturned.

    f -- file to be parsed
    start_delimiter -- Delimiter to mark starting point. Regex String
    end_delimiter -- Delimiter to mark end point. Regex String
    verbose -- default is false, set output to verbose
    """
    if verbose:
        print("Parsing " + str(f) + " for delimiters with")
        print("Start Delimiter: " + start_delimiter)
        print("End Delimiter: " + end_delimiter + "\n\n")
    f1 = open(f)
    read = False
    parsed_content = ""
    for line in f1.readlines():
        if read:
            if verbose:
                print(line)
            parsed_content = parsed_content + line + "\n"
        if re.match(start_delimiter, line) or re.match(end_delimiter, line):
            if verbose:
                print("Delimiter: " + line)
            read = not read

    if verbose:
        print("Parsed " + str(f))

    if parsed_content == "":
        print("\033[31mNo Delimiters in " + str(f))
        return 0

    return io.StringIO(parsed_content)

def read(data, data2, key, verbose=False):
    """
    Read yml vars recursively and check vars of data1 and data2 for equality

    data1 -- data to check equality. Keys of this file are used and searched in data2
    data2 -- data to check equality
    key -- current key who is checked
    verbose -- default is false, set output to verbose
    """

    if type(data) is dict:
        c = 0
        for k in data.keys():
            if not k in data2.keys():
                print('\033[31m' + k + ' does not exist in ' + str(file2), file=stderr)
                return 1
            c += read(data=data[k], data2=data2[k], key=key + k + "/", verbose=verbose)
        return c
    else:
        return not check(var1=data, var2=data2, key=key, verbose=verbose)


def check(var1, var2, key, verbose=False):
    """
    check two variables for equality

    var1 -- variable to check
    var2 -- variable to check
    key -- key of the variables
    verbose -- set output to verbose, default is false
    """
    if key in omit_keys:
        return 1
    if verbose:
        print('\033[0mChecking Key \"' + key + '\" with values \"' + str(var1) + '\" and \"' + str(var2) + '\"\n')
    if var1 != var2:
        print('\033[31mError in Key ' + key + ':\n\033[0m' + str(var1) + ' is not equal to ' + str(var2) + '\n',
              file=stderr)
        return 0
    return 1

# Script
# Argument Parsing

parser = argparse.ArgumentParser(description="""
This script compares variables between two YAML files. You have two options to specify which keys to compare:

1. Use delimiters in the YAML files.
2. Provide a separate file with a list of keys to check.

By default, the script uses delimiters to identify which keys to compare. If you choose to provide a 'keys_to_check' file, ensure that it contains a list of keys under the 'keys_to_check' field.

Additionally, you can provide a 'omit_keys_file' to specify keys that should be ignored during the comparison, ensure that it contains a list of keys under the 'omit_keys' field..

Examples:
1. Compare using delimiters:
   python script_name.py file1.yml file2.yml

2. Compare using a keys_to_check file:
   python script_name.py file1.yml file2.yml -k keys_to_check.yml

3. Compare using delimiters but omit certain keys:
   python script_name.py file1.yml file2.yml -o omit_keys.yml
""", formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('file1', type=str, help='Path to the first YAML file. Need to have delimiters in delimiter mode.')
parser.add_argument('file2', type=str, help='Path to the second YAML file')
parser.add_argument('-s', '--start_delimiter', type=str, help='Start delimiter for variables to compare as regex (default: "^###\ Begin.*parameters\ ###$")')
parser.add_argument('-e', '--end_delimiter', type=str, help='End delimiter for variables to compare as regex (default: "^###\ End.*parameters\ ###$")')
parser.add_argument('-o', '--omit_keys_file', type=str, help='Path to .yml file containing keys to omit from comparison. Keys should be in a list under "omit_keys" field')
parser.add_argument('-k', '--keys_to_check_file', type=str, help='Path to .yml file containing keys to check. Use this instead of delimiters. Keys should be in a list under "keys_to_check" field. This Option deactivates delimiters')
parser.add_argument('-v', '--verbose', action='store_true', help='Display verbose output')
args = parser.parse_args()

start_delim = "^###\ Begin.*parameters\ ###$" if not args.start_delimiter else args.start_delimiter
end_delim = "^###\ End.*parameters\ ###$" if not args.end_delimiter else args.end_delimiter

omit_keys = []
keys_to_check = None

# Ensure either delimiter or keys_to_check_file is provided, but not both
if args.keys_to_check_file and (args.start_delimiter or args.end_delimiter):
    print('\033[31mError: Either provide delimiters or a keys_to_check_file, but not both.\033[0m')
    raise SystemExit(1)

# Check for legal files
path_exists(args.file1)

path_exists(args.file2)

if args.omit_keys_file:
    path_exists(args.omit_keys_file)

    # Load omit_keys from the provided file
    with open(args.omit_keys_file, 'r') as file:
        omit_keys_data = yaml.load(file, Loader=SafeLoader)

    omit_keys = omit_keys_data.get('omit_keys', [])

if args.keys_to_check_file:
    path_exists(args.keys_to_check_file)

    # Load keys to check from the provided file
    with open(args.keys_to_check_file, 'r') as file:
        keys_to_check_data = yaml.load(file, Loader=SafeLoader)

    keys_to_check = keys_to_check_data.get('keys_to_check', [])

print('\033[0m###### Checking ' + str(args.file1) + ' and ' + str(args.file2) + (f' against key file {args.keys_to_check_file}' if keys_to_check else ' with delimiters') + '. ######')

# open files and parse yml
data_file1 = open(args.file1) if keys_to_check else load_yaml_delim(args.file1, start_delim, end_delim, args.verbose)
data_file2 = open(args.file2)

if not data_file1:
    print("\033[31mError in Parsing " + str(args.file1), file=stderr)
    raise SystemExit(1)

data_file1 = yaml.load(data_file1, Loader=SafeLoader)
data_file2 = yaml.load(data_file2, Loader=SafeLoader)

cnt = 0

if keys_to_check:
    # Check if the keys from keys_to_check are present in both files
    missing_keys_in_file1 = check_missing_keys(data_file1, keys_to_check, args.file1)
    missing_keys_in_file2 = check_missing_keys(data_file2, keys_to_check, args.file2)

    if not missing_keys_in_file1 and not missing_keys_in_file2:
        for key in keys_to_check:
            cnt += read(data_file1[key], data_file2[key], key +"/", args.verbose)
    else:
        raise SystemExit(1)
else:
    # If keys_to_check is not provided, use the existing logic to check keys between delimiters
    cnt = read(data_file1, data_file2, "", args.verbose)

# Ending message of program
if cnt <= 0:
    print('\033[32m###### Success. ' + str(args.file1) + ' and ' + str(args.file2) + ' are equal. ######\n\033[0m')
    raise SystemExit(0)
else:
    print('\033[31m###### ' + str(cnt) + ' different variables in ' + str(args.file1) + ' and ' + str(args.file2) + ' ######\n\033[0m', file=stderr)
    raise SystemExit(1)