.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Firewall
========
It's a good idea to only open the ports you really need - especially for incoming traffic.
In the most simple setup of one server containing all services you probably want 22 & 443 open for SSH & HTTPS and deny everything else.
On Debian systems this can be accomplished using nftables.

This repository contains a simple nftables role for that. It is described in the roles chapter.

The firewall role is not meant to get very complex as firewall needs are very individual and on multiple layers anyway.
If you want to extend its functionality for distributed servers you should probably move some differing variables from `group_vars` to `host_vars`.
You can read more about `host_vars` here:

https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#organizing-host-and-group-variables

If GNU Health were distributed on multiple systems you would at least want to allow the following (assuming default ports):

- PostgreSQL:
    - Allow 5432 ingress, only coming from GNU Health
    - Allow 22 ingress, coming from Ansible controller

- GNU Health:
    - Allow 5432 egress, only to PostgreSQL
    - Allow 8443 (or 8000) ingress, only coming from Nginx
    - Allow 22 ingress, coming from Ansible controller

- Nginx:
    - Allow 8443 (or 8000) egress, only to GNU Health
    - Allow 443 (& 80) ingress, only coming from your hospital (sub-)network
    - Allow 22 ingress, coming from Ansible controller

Note that the backup server with barman and restic already complicates the demand of ports as there are both SSH and PostgreSQL connections needed. The monitoring system Zabbix and your own Certificate Authority (CA) could also be additional systems. This would lead to the following:

- PostgreSQL:
    - Allow 5432 ingress, coming from GNU Health
    - Allow 22 egress, only to backup server
    - Allow 5432 egress, only to backup server
    - Allow 5432 ingress, coming from backup server
    - Allow 22 ingress, coming from backup server and Ansible controller
    - Allow 10050 ingress, coming from Zabbix server

- GNU Health:
    - Allow 5432 egress, only to PostgreSQL
    - Allow 8443 ingress, only coming from Nginx
    - Allow 22 ingress, coming from Ansible controller
    - Allow 22 egress, only to backup server
    - Allow 10050 ingress, coming from Zabbix server

- Nginx:
    - Allow 8443 egress, only to GNU Health
    - Allow 443 (& 80) ingress, only coming from your hospital (sub-)network
    - Allow 22 ingress, coming from Ansible controller
    - Allow 10050 ingress, coming from Zabbix server

- Backup:
    - Allow 22 egress, only to PostgreSQL
    - Allow 5432 egress, only to PostgreSQL
    - Allow 22 ingress, coming from GNU Health, PostgreSQL and Ansible controller
    - Allow 5432 ingress, coming from PostgreSQL
    - Allow 10050 ingress, coming from Zabbix server

- Zabbix:
    - Allow 8080 ingress, coming from Ansible controller or administration subnet
    - Allow 10050 egress, to all agents
    - Allow 22 ingress, coming from Ansible controller

- Certificate Authority:
    - Allow 22 ingress, coming from Ansible controller
    - Allow 10050 ingress, coming from Zabbix server

- Work stations:
    - Allow 22 ingress, coming from Ansible controller
    - Allow 10050 ingress, coming from Zabbix server
    - Allow 443 egress, to Nginx

If you plan to restrict outgoing traffic you should also consider the one related to incoming traffic as responses, ports used by package installations, potential email notifications and any further functionalities you are using.
