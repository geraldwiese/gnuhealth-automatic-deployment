# SPDX-FileCopyrightText: 2023 Jonathan Feilmeier
#
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml
import argparse
import re

from pathlib import Path
from sys import stderr
from sys import exit

def path_exists(path):
    path = Path(path)

    if not path.exists():
        print('\033[31m' + str(path) + ' does not exist', file=stderr)
        raise SystemExit(1)

def extract_section_content(rst_content, sections):
    # Check if a specific section is defined
    if sections:
        sections_content = {}
        for section in sections:
            pattern = re.compile(fr"- \*\*{re.escape(section)}\*\*:\n\n(.*?)(\n\n|$)", re.DOTALL)
            match = pattern.search(rst_content)
            if not match:
                print('\033[33m' + 'WARNING: Could not find section ' + section + ' in the RST file')
                sections_content[section] = []
                continue
            section_content = match.group(1).strip()
            sections_content[section] = section_content
        return sections_content
    else:
        # If no specific section is defined, return the entire content
        return rst_content

def check_documentation(yaml_file_path, rst_file_path, section=None):
    # Read the YAML file and extract variable names
    with open(yaml_file_path, 'r') as file:
        yaml_content = yaml.safe_load(file)
    variable_names = list(yaml_content.keys())

    # Read the RST file
    with open(rst_file_path, 'r') as file:
        rst_content = file.read()

    # Extract the content of the specified section (if defined)
    content_to_check = extract_section_content(rst_content, section)

    undocumented_variables_per_section = {}

    # Check which variable names are not documented in the content
    for section, content in content_to_check.items():
        undocumented_variables_per_section[section] = [var for var in variable_names if var not in content]

    # Preperation for interesection. initialize set with first value_list
    undocumented_set = set(undocumented_variables_per_section[next(iter(undocumented_variables_per_section))])

    for key, value in undocumented_variables_per_section.items():
        undocumented_set = undocumented_set.intersection(set(value))

    return list(undocumented_set)

# Create ArgumentParser object
parser = argparse.ArgumentParser(description='Check if all variables from the YAML file are documented in the RST file.', formatter_class=argparse.RawTextHelpFormatter)

# Add arguments
parser.add_argument('yaml_file_path', type=str, help='Path to the YAML file')
parser.add_argument('rst_file_path', type=str, help='Path to the RST file')
parser.add_argument('--section', type=str, nargs='+', help='''Name of the sections in the RST file to check.
                    A section in the RST file is expected to be in the format:
                    "- **<SectionName>**:\\n\\n"

                    End of Section is defined as "\\n\\n".

                    Followed by the content of the section. If this argument is not specified,
                    the entire file is checked.''')
# Parse arguments
args = parser.parse_args()

path_exists(args.yaml_file_path)
path_exists(args.rst_file_path)

print('\033[0m###### Checking documentation of ' + str(args.yaml_file_path) + ' in ' + str(args.rst_file_path) + '. ######')

# Perform the check
undocumented_variables = check_documentation(args.yaml_file_path, args.rst_file_path, args.section)
# Print the result
if undocumented_variables:
    print("\033[31m###### Undocumented variables:\033[0m", file=stderr)
    for var in undocumented_variables:
        print("-", var)
    raise SystemExit(1)
else:
    print("\033[32m###### All variables are documented. ######\n\033[0m")
    raise SystemExit(0)