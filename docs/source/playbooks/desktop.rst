.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

desktop
=======

This playbook is meant to manage workstations and mainly to install the GNU Health client.

The following booleans in `ìnventories/dev/group_vars/desktop` control whether sub steps are executed are not:

- **gnuhealth_client** (default: true)

- **ghcl_trust_certs** (default: false)

- **gpg** (default: false)

- **mygnuhealth** (default: false)

- **unattended-upgrades** (default: false)

- **timezone** (default: false)

The boolean `ghcl_trust_certs` controls if the certificate role is included in order to trust the certificate of the GNU Health server so the client can connect. This only happens if the boolean `gnuhealth_client` is true as well.

All other booleans above call roles with the same name as the boolean itself.
