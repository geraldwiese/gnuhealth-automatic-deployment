.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

barman
======

This role install and configures Barman:

"Barman (Backup and Recovery Manager) is an open-source administration tool for disaster recovery of PostgreSQL servers written in Python. It allows your organisation to perform remote backups of multiple servers in business critical environments to reduce risk and help DBAs during the recovery phase.

Barman is distributed under GNU GPL 3 and maintained by EnterpriseDB, a platinum sponsor of the PostgreSQL project."

https://docs.pgbarman.org/release/3.8.0/

Here the scenario 1b with streaming backup and WAL archiving is realized.

If `psql_barman_sync` is set to true in addition this should enable to achieve a Recovery Point Objective (RPO) of zero - according to the documentation linked above.

Note that this is only implemented to work on distinct systems as the backup as your backup system should be a separate system anyway, especially when disaster scenarios are meant to be covered.

As stated in the documentation Barman does not implement partial point in time recovery yet. The default values set the full point in time recovery to 1 month, create base backups every month and keep at least 3 base backups. That way you have partial point in time recovery but if manual base backups are taken it is broken and only have at least 1 month again. Anyway this solution does not obsolete archiving or high availability. It mainly offers a high standard for backups and restore.

The restore does only work on empty directories as this is not intended to overwrite any data that might not be saved.

The following variables can be configured:

- **Parameters**:

  - brm_psql_host: The domain name of the PostgreSQL server. Used for PostgreSQL streaming and SSH connectivity between the servers and restore command.
  - brm_psql_super_user: The PostgreSQL super user that is used by Barman for administrative tasks.
  - brm_psql_replication_user: The PostgreSQL replication user that is used by Barman for continuous streaming.
  - brm_psql_super_pw: The password of the super user
  - brm_psql_replication_pw: The password of the replication user
  - brm_psql_config_name: The config name that is used by Barman to identify the backup scenario
  - brm_retention_window: Time span to keep the WAL files
  - brm_min_backups: Minimum number of base backups to keep. This prevents backup deletion also for manual commands.
  - brm_max_backup_age: Maximum age of the last base backup
  - brm_cron_base_backup: Whether base backups are run by Cron
  - brm_cron_base_backup_period: Period for making base backups
  - brm_trust_ssh_key: Dictionary where the `key` specifies a path with a SSH key that should be trusted for the `user`
  - brm_ssh_port: SSH port of Barman
  - brm_ssh_port_psql: SSH port of `brm_psql_host`
  - brm_restore: Perform restore if true
  - brm_restore_handle_service: Stop PostgreSQL service before and start it after restore
  - brm_restore_psql_dir: Path of directory to restore the cluster into
  - brm_restore_cmd: Restore command containing hostname, port, point of time, etc.

- **Defaults**:

  - brm_install_only: This controls if only the package is installed or configuration steps are executed as well. This is used because some configuration steps need SSH connectivity but the steps for SSH connectivity need the `barman` user created by the barman package.
  - brm_host: Hostname of barman for restore purpose, taken from inventory by default

- **Variables**:

  - brm_packages: The packages to be installed system wide needed by the role
  - brm_package_names: Used by the variable `brm_packages` when opearting systems have different package names
  - brm_conf_dir: Directory for config files
  - brm_home: Home directory of barman user e.g. for SSH
  - brm_user: Name of the OS user
  - brm_streaming_archiver_name: Name of the WAL streaming archiver
  - brm_debian_fix: If true repair postgresql.auto.conf after restore before restart of PostgreSQL service because the "--port" argument is missing
