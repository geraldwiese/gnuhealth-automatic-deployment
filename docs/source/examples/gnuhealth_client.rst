.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

GNU Health client without server
================================

Follow this example if you want to have the client without a server on the same system.
Instead you can also connect to the demo server, an instance you installed on another system or even on your same system without Ansible.

Compared to the previous example you only need to modify the way you call `ansible-playbook`:

* Follow the steps of the first example for requirements but do not run any `ansible-playbook` command

* Run the Playbook `desktop.yml` like this:`

.. code-block:: console

    $ ansible-playbook playbooks/desktop.yml -c local -i inventories/dev/ -e ansible_user=`whoami` -e ghcl_include_his_vars="" -K

* Again you can run the client either using the desktop entry or from terminal:

.. code-block:: console

    $ gnuhealth-client

* Hit `Connect` and enter the password `gnusolidario`.
