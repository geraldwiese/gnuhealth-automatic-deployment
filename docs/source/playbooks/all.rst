.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

all
===

This playbook is intended to combine the minimal steps for GNU Health, Orthanc & Thalamus installation.

See the example for multiple applications on one system for instructions how to use it.
