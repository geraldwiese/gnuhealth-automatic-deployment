<!--
SPDX-FileCopyrightText: 2023 Gerald Wiese

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Migrated to Codeberg
**This project was migrated to the new GNU Health repositories on Codeberg:**

https://codeberg.org/gnuhealth/ansible

# Automatic Deployment of the open source hospital information system GNU Health using Ansible
The Ansible playbooks in this project aim to automatically deploy the hospital information system GNU Health including the HIS node, Thalamus, the DICOM server Orthanc and a desktop environment as a client using the GNU Health HIS node.  
**HIS node**: Hospital Information System, core of the GNU Health system  
**Orthanc**: The DICOM server is not directly part of the GNU Health system but its integration is provided  
**Thalamus**: GNU Health Federation (Development in progress)  
Supported operating systems are the latest Debian, Ubuntu LTS, openSUSE Leap and FreeBSD.  

# Main functionality
The main steps of the playbooks for installing GNU Health are the following:
- Install dependencies and main PyPI package
- Configure uWSGI
- Create systemd service(s)
- Install and configure PostgreSQL & Nginx
- Create database user and database
- Create a reverse proxy
- Install the GNU Health client on any number of machines, enter the servers connection information meanwhile

If administration procedures are added you can also:
- Create or handle digital certificates
- Use Barman & Restic for backup/restore solutions
- Use Logwatch or Zabbix for monitoring
- Configure firewall rules using nftables
- Check the documentation for further options

This project is not intended to work in a dockerized environment but is rather expected to be used in Virtual Machines.
If you want different systems for different services it is possible to separate PostgreSQL, Nginx, a backup server, a monitoring server and a custom Certificate Authority from the actual application GNU Health.

For a quickstart jump to the section 'Examples' in the documentation.

Note that this project is still developed on GitLab but the main branch is synchronized into the official GNU Health repositories.  

GitLab repository & documentation:  
https://gitlab.com/geraldwiese/gnuhealth-automatic-deployment  
https://geraldwiese.gitlab.io/gnuhealth-automatic-deployment/  

Mercurial repository & documentation:  
https://hg.savannah.gnu.org/hgweb/health-ansible/  
https://docs.gnuhealth.org/ansible/


# Links
Ansible documentation https://docs.ansible.com/  
GNU Health Documentation Portal https://www.gnuhealth.org/docs/  
Orthanc Book https://book.orthanc-server.com/  
PostgreSQL documentation https://www.postgresql.org/docs/  
Nginx documentation https://nginx.org/en/docs/  
