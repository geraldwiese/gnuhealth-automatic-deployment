.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Overview
========

The deployment of GNU Health and other applications is automated in a modular way.
Thus the same scripts ca be used for an installation without virtualization, on one Virtual Machine (VM) or distributed on multiple VMs in order to have one distinct system per functionality. The latter case is visualized in the following graphic:

.. image:: graphics/overview.png

This is realized using the automation and configuration management tool **Ansible**.

For every type of deployment there are `.yml` files in the **playbooks** directory.
Those are the main scripts that you can execute.

Additionally there are Ansible roles with the corresponding names and other **roles** getting called implicitly.
They are containing the tasks that will install packages and perform configuration steps for creating the desired service.

| Your individual configuration is managed in the different **inventories** folders.
| They can be used for having different develop, test & production environments.
  In this repository only the `dev` inventory is filled.
  It can be used as a base for a test or prod environment.
| Inside `dev` is a subfolder `group_vars` where you will find directories for different deployments each containing two configuration files `vars.yml` and `vault.yml`.
  The first one contains the main configuration and the second one credentials.
  Thus it is meant to be encrypted using Ansible Vault (see Securing SSH & passwords).
| Some default variables are only stored in roles/<name>/defaults/main.yml. Usually they do not have to be modified but especially if you want to reuse single roles you
  will need them.
| Finally the connection informations of the target hosts are given in `hosts` inside inventories folders.

The **vagrant** folder contains Vagrantfiles and inventory files for overriding the `hosts` file mentionned above.
Those can be used to set up VMs using VirtualBox for different scenarios.

If files are needed by different systems those are put into the **fetch** folder.
For some cases of handling certificates files have to be transferred from one system to another.
Besides the desktop systems having the GNU Health client installed need connection parameters of the server.
Those are also written into this folder.

Finally there are **tests** that are constantly used and **docs** which you are currently reading.

For a **quickstart** you can switch into the chapter `Examples`.

| This repository contains automated solutions with various configuration options for installation and administration procedures.
| **Installation** refers to calling the playbooks with the `_minimal` suffix and keeping the default configuration as in the first example.
| **Administration** means extending the functionality by system administration procedures like certificates, backup/restore, etc.
| The installation is continuously tested for the latest Debian, Ubuntu LTS, openSUSE Leap and FreeBSD.
  However the administration features are mainly developed, tested and used for Debian. For productive settings using various roles and configuration options it is currently recommended to use Debian. Contributions to improving the advanced scenarios for other OS are more than welcome!

Until here it is possible to split your GNU Health (or Orthanc) system into the actual application, PostgreSQL, Nginx, a custom Certificate Authority (CA) and a backup system.
If you want all of them to be different systems your `hosts` file should be modified to match the following snippet.
Replace the <placeholders> by your actual domain name or IP address.
The variable `ansible_port` is put so you could set a custom SSH port.
By default there is one uncommented entry in the top level group leading to all in one system.

::

    [gnuhealth]
    # outcommented ansible_port=22

    [...]

    [gnuhealth_app]
    <domain> ansible_port=22

    [gnuhealth_psql]
    <domain> ansible_port=22

    [gnuhealth_nginx]
    <domain> ansible_port=22

    [gnuhealth_ownca]
    <domain> ansible_port=22

    [gnuhealth_bak]
    <domain> ansible_port=22


You can modify `ansible.cfg` in the top level directory if you wish to change your Ansible configuration options.
