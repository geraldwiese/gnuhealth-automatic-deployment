.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

uwsgi
=====

| uWSGI is an open source web server that implements the Python Web Server Gateway Interface (WSGI, see PEP 3333).
| The use of uWSGI gives many configuration options like a dynamic amount of workers which increases the robustness for productive use.
| There are three connection methods between uWSGI and Nginx:

- If Nginx is on the same system a UNIX socket will be used.
  Therefore a group `uwsginginx` is created and both `gnuhealth` & `ẁwww-data` have to be part of that group.
  Otherwise you would either have a world-writable socket or could not access your server.

- Besides the uwsgi protocol can be used by Nginx if systems are separated but the intranet considered secure.

- Alternatively uWSGI can also provide HTTPS.

The uWSGI configuration contains two parts: Necessary steps for getting it running and recommendations for productive use of this source:

https://www.bloomberg.com/company/stories/configuring-uwsgi-production-deployment/

The template `uwsgi.ini.j2` contains many configuration options that were already commented inside.
Worth to note is the harakiri option: It kills workers if a request exceeds the threshold of currently 600s.
This is intended to prevent having numerous idle workers but could raise problems as well.

Finally uWSGI will be managed by systemd: A .service file will be copied and the service enabled so it always starts after booting.

| uWSGI documentation:
| https://uwsgi-docs.readthedocs.io/en/latest/

- **Parameters**:

  - uwsgi_instances: List of uWSGI instances to deploy, selections are provided in defaults
  - uwsgi_upgrade: If true pass `latest` to Ansibles pip module instead of `present`

- **Defaults**:

  - uwsgi_cert_filename: Path for certificate for HTTPS
  - uwsgi_key_filename: Path for key for HTTPS
  - uwsgi_template_comment: Comment put on top of every template delivered by Ansible
  - uwsgi_example_instance: Dictionary containing all parameters that are possibly used by the role, no working default config, just to show what can be configured.
    See `gnuhealth` and `thalamus` roles defaults for example configurations.

- **Variables**:

  - uwsgi_packages: Packages to be installed on the system differentiated by OS family
  - uwsgi_nginx_group_name: Name of the group to be created for Nginx user & uWSGI user in order to have proper UNIX socket permissions (if necessary)
  - uwsgi_become_method_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - uwsgi_become_exe_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - uwsgi_become_flags_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
