.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

thalamus
========

The GNU Health Federation aims to create a network of multiple HIS servers.
Thalamus is the backend to create this federation.
It is still in development and no frontend exists yet to e.g. show statistics.

The installation strategy is very similar to the HIS in terms of the directory structure, pip installation inside a virtual environment and putting uWSGI in front despite that `gnuhealth` is replaced by `thalamus`.

| You can find the documentation of Thalamus here:
| https://www.gnuhealth.org/docs/thalamus/

- **Parameters**:

  - th_version: Version for pip installation
  - th_psql_uri: PostgreSQL connection URI
  - th_populate_db: If true create demo data in database
  - th_flask_secret_key: Secret key for Flask app
  - th_salt: Salt used when putting flask secret key
  - th_ssh_key: If true create a SSH key for `thalamus` user

- **Defaults**:

  - th_home_path: Home directory of `thalamus` user
  - th_config_path: Directory for config files
  - th_log_path: Directory for log files
  - th_run_path: Run directory
  - th_tls_path: TLS directory
  - th_venv: Virtual environments path
  - th_use_testpypi: If true use TestPyPI for testing pip package
  - th_https: Prepare HTTPS for uWSGI if true
  - th_cert: Path of server sertificate
  - th_key: Path of private key
  - th_psql_verify_tls: If true, verifiy PostgreSQL TLS
  - th_psql_cert_path: Path of certificate for TLS verification of PostgreSQL
  - th_psql_db_user: Name of PostgreSQL user
  - th_psql_db_name: Name of PostgreSQL database
  - th_ssh_dir: Directory for `thalamus` users SSH, will be created if not present
  - th_ssh_key_path: Path of the private SSH key (public will be the same + '.pub')
  - th_ssh_key_remote_user: Remote user that is target of SSH connection for backups
  - th_uwsgi_unix_socket: Whether uWSGI should spawn a UNIX socket (otherwise TCP and either uwsgi protocol or HTTPS if `th_https` is true)
  - th_uwsgi_instance: Dictionary containing all the parameters passed to uWSGI role

- **Variables**:

  - th_packages: Packages to be installed on the system differentiated by OS family
  - th_become_method_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - th_become_exe_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
  - th_become_flags_unprivileged_users: Set to make become_user work on Suse and to Ansible default otherwise
