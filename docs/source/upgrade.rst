.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Upgrade
=======


PyPI packages
-------------

The GNU Health Hospital Information System (HIS) is installed using PyPI. There are three ways to upgrade:

- Adjusting the variable `gh_version` and running the playbook again

- Enabling `gh_update_weekly` for weekly automatic updates - only for minor versions

- Manually enabling the virtual environment `gh_venv` and upgrading the package

If you enable `gh_update_weekly` be careful to update `gh_version` before running the playbook again.

For the corresponding client, Thalamus und MyGNUHealth only the first and third option are available. The variables are called `ghcl_version`, `th_version` and `mygh_version`.

Regarding uWSGI you have two options:

- Setting `uwsgi_upgrade` to true

- Manually enabling the virtual environment given as `venv` key in the `uwsgi_instances` list of dictionaries and upgrading the package


System packages
---------------

PostgreSQL, Orthanc, Nginx and other packages are installed by the package manager of the OS. Updates and upgrades work as always for the OS.

Note that major upgrades of your packages like PostgreSQL or Orthanc might require more effort and a backup before.

Orthanc upgrade documentation:

https://book.orthanc-server.com/users/replication.html

PostgreSQL upgrade documentation:

https://www.postgresql.org/docs/15/upgrading.html

There is no playbook for simple standard upgrades in a manual way but those can also be executed using Ansibles ad hoc commands. The following would run cache updates, upgrades and reboots on all defined hosts in your inventory (command Debian based):

.. code-block:: console

    $ ansible -i inventories/dev/ -m command -a "apt update" --become -K all
    $ ansible -i inventories/dev/ -m command -a "apt -y upgrade" --become -K all
    $ ansible -i inventories/dev/ -m command -a "reboot" --become -K all

Of course you could also reduce affected hosts by replacing `all` by your group, replace apt by another package manager or leave `-K` if you have passwordless sudo.

Besides there is a role `unattended-upgrades` for automatic upgrades on Debian based systems.

Database schemes
----------------

When GNU Health publishes a new HIS major version you usually need to upgrade the database scheme and a .sql file is published to realize this.

Until here this process is not integrated into Ansible.

GNU Health HIS upgrade documentation:

https://docs.gnuhealth.org/his/his/upgrade.html


Ansible repository
------------------

No backwards compatibility is guaranteed. It is recommended to keep an archive of this Ansible repository before and after your modifications anyway.
If a new Ansible version is released and you want to integrate it in your running system, use the `diff` command to investigate how to merge your own configuration or modification and the next release.

Ansible
-------

Depending on the way you installed Ansible itself. If it was pipx you can upgrade it the following way:

.. code-block:: console

    $ pipx upgrade --include-injected ansible-core

Or if it was installed using pip:

.. code-block:: console

    $ pip install --user --upgrade ansible-core

Those are preferred over operating system packages as you receive bug fixes and new functionalities faster.
