.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

locale
======

This role can be used to control the systems locales.

The role comes with the following configuration options:

- **Parameters**:

  - lcl_install: List of languages to install
  - lcl_set: List of name <-> value pairs for setting locales
  - lcl_path: Path of config file to put definitions from above at
