.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

orthanc
=======

This playbook works exactly as the one of gnuhealth except that the group of hosts and the role are called `orthanc` instead of `gnuhealth` and no uWSGI role is needed because Orthanc already comes with a HTTP server.

Check the Orthanc role for specific information.
