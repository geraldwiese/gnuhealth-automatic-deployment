.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

gnuhealth
=========

The playbook `gnuhealth.yml` installs the GNU Health Hospital Information System (HIS) which is the core of GNU Health.

The following booleans in `ìnventories/dev/group_vars/gnuhealth` control whether sub steps are executed are not:

- **certificate_authority** (default: false)

- **postgresql** (default: true)

- **application** (default: true)

- **nginx** (default: true)

- **backup** (default: false)

- **fail2ban** (default: false)

- **unattended_upgrades** (default: false)

- **ssmtp** (default: false)

- **systemd_alert** (default: false)

- **timezone** (default: false)

- **nftables** (default: false)

If you call the `_minimal` playbook the booleans are ignored anyway and only the roles for the sub steps which are true by default get included. Configuration steps for SSH or TLS for distributed systems are also skipped.

The minimal playbook contains the following roles: `check`, `postgresql`, `gnuhealth`, `uwsgi`, `nginx` and `gnuhealth-client`. The latter usually belongs to the `desktop` playbook but the minimal scenario is expected to be everything in one system anyway thus it also calls the role for the client to connect to the server.

Check the chapter `Roles` for details about all the integrated roles.
