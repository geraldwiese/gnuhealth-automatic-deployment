��          |      �          �   !  -   �          1     C     ^  ;   r  6   �  3   �  <        V  "  t  �   �  $   `     �     �     �     �  1   �  +     !   ?  >   a     �                                          
          	       ========== GNU Health Camera Help ==========

a = Attach media file in the current model

[space] = Set the picture in model (when available)

h = This help message

q = Quit the camera application

 Attachment is saved, Please refresh the view! CV2 library is not found! GNU Health camera Multiple records selectd ! No Camera is found! Please choose one record associated to this picture / video Please choose only one record for this picture / video Please ensure that your camera is working properly. Please install "opencv-python" and restart gnuhealth-client. You need to select a record ! Project-Id-Version: gnuhealth-plugin-gnuhealth_camera 4.4.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-21 09:56+0800
Last-Translator: Automatically generated
Language-Team: none
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 ========== GNU Health 摄像头帮助 ==========

a = 将多媒体文件添加到当前模型的附件中

[space] = 设置模型照片（如果可以）

h = 显示帮助信息

q = 退出摄像头

 附件已保存，请刷新视图！ 未找到 CV2 库！ GNU Health 摄像头 记录选择超过一条！ 未找到摄像头！ 请选择一条与此图片/视频相关的记录 请为此图片/视频只选择一个记录 请确保摄像头正常工作。 请安装 opencv-python 包然后重启 gnuhealth 客户端。 你需要选择一条记录！ 