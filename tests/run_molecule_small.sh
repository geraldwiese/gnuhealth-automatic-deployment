#! /bin/bash

# SPDX-FileCopyrightText: 2023 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Run this from top level directory like this:
# bash tests/run_molecule_small.sh | tee test.log

count_test() {
    let COUNTER_TOTAL++
    echo $TEST_STRING
    if "$@"; then
        let COUNTER_SUCCESS++
    else
        let COUNTER_FAILED++
        echo "${TEST_STRING} failed" >> $FAIL_LOG
    fi
}

# Prepare
cd tests/playbook_allinone
COUNTER_TOTAL=0
COUNTER_FAILED=0
COUNTER_SUCCESS=0
FAIL_LOG="../../fail.log"

[ ! -f $FAIL_LOG ] || rm $FAIL_LOG
# Test example 1 (+ Orthanc similarly) & example 2
for playbook in "gnuhealth" "desktop"; do
    export PLAYBOOK_TO_TEST=${playbook}
    TEST_STRING="Testing playbook playbooks/${playbook}.yml for all in one system"
    count_test molecule test
done

# Conclude
echo "total: ${COUNTER_TOTAL}"
echo "failed: ${COUNTER_FAILED}"
echo "success: ${COUNTER_SUCCESS}"
