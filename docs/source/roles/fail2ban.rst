.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

fail2ban
========

Fail2Ban monitors malicious network behaviour in log files and bans IPs temporarily if their behaviour matches specific filters (putting IPs in so called "jails").
After installation it is only enabled for SSH. The scripts enable some filters for nginx as well.
Banning IP addresses is controlled by `f2b_banaction` which is `nftables-allports` by default.
You can search for bans with the following command:

`sudo zgrep 'Ban' /var/log/fail2ban.log*`

https://www.fail2ban.org/

- **Parameters**:

  - f2b_banaction: Action used by Fail2Ban to ban IPs
  - f2b_use_mail: Whether email notifications should be used (requires sendmail)
  - f2b_admin_mail: Email address that should receive email notifications
  - f2b_nginx: Whether Nginx jails should be used
  - f2b_nginx_ports: Ports for the Nginx jails
  
- **Defaults**:

  - f2b_template_comment: Comment put on top of every template delivered by Ansible
  - f2b_debian_fix: If using Debian, install `python3-systemd` and set `backend = systemd` for the sshd jail, see https://github.com/fail2ban/fail2ban/issues/3292#issuecomment-1678844644
  - f2b_detect_key_failure: Set `publickey = any` in order to ban after key based authentication failures as well
