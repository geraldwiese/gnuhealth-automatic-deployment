.. SPDX-FileCopyrightText: 2023 Gerald Wiese
..
.. SPDX-License-Identifier: GPL-3.0-or-later

postfix
=======

This role can be used to realize email notifications by using an email gateway. Note that this role is not intended to implement any postfix functionality apart from using an existing gateway.
Installation of postfix is controlled by the boolean `postfix`.
Configuration options are set using `debconf` and in the configuration files.

The role comes with the following configuration options:

- **Parameters**:

  - pf_mailname: The name that is used in emails where it comes from, by default the host name
  - pf_mailtype: Only 'Satellite system' is supported until here
  - pf_relayhost: The domain name of the email gateway you want to use
  - pf_mailto: The email address for receiving emails, assigned to root in /etc/aliases

- **Variables**:

  - pf_main_config_path: The path of the main config file main.cf
